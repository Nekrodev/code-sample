import React from "react";
import { Box } from "../../../../common/components/Layout/Box";
import { InnerBox } from "../../../../common/components/InnerBox";
import { LargeHeading } from "../../../../common/components/TextElements";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import translations from "./translations";
import { ShadowedBox } from "../../../../common/components/ShadowedBox";
import NewAmbassadorCode from "./NewAmbassadorCode";
import AmbassadorCodesList from "./AmbassadorCodesList";

const AmbassadorCodes: React.FC = () => {
  const translate = useTranslate();

  return (
    <Box flex>
      <InnerBox as={ShadowedBox} padding={2.5}>
        <Box>
          <LargeHeading>{translate(translations.Title)}</LargeHeading>
          <Box my={4}>
            {translate(translations.text1)}
            <br />
            <br />
            {translate(translations.text2)}
            <br />
            <br />
            {translate(translations.text3)}
            <br />
            <br />
            {translate(translations.text4)}
          </Box>
        </Box>

        <AmbassadorCodesList />

        <hr />

        <NewAmbassadorCode />
      </InnerBox>
    </Box>
  );
};

export default AmbassadorCodes;
