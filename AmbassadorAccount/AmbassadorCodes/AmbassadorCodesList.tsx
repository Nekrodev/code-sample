import React, { useContext } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import copy from "copy-to-clipboard";
import { AmbassadorCode } from "../../../types/AmbassadorCode";
import { Columns, Column } from "../../../../common/components/Layout/Col";
import FormSimpleItem from "../../../../common/components/Form/SimpleItem";
import { Box } from "../../../../common/components/Layout/Box";
import {
  useTranslate,
  TranslationContext,
} from "../../../../common/containers/TranslationProvider";
import translations from "./translations";
import { Icon } from "../../../../common/components/Icon";
import { getAmbassadorCodes } from "../../../selectors/codes";
import { useDeleteAmbassadorCodeModal } from "./DeleteAmbassadorCodeModal";
import { getLanguageRouteParam } from "../../../../routing/useNavigate";
import { Language } from "../../../../common/types/Translations";

const Text = styled.div`
  margin-top: 1.1rem;

  @media screen and (max-width: 768px) {
    margin-top: 0;
  }
`;

const IconsColumn = styled(Column)`
  display: flex !important;
  justify-content: space-between;
  padding-top: 1.6rem !important;

  @media screen and (max-width: 768px) {
    padding-top: 0.5rem !important;
    justify-content: space-around;
  }
`;

const CodeColumn = styled(Column)`
  @media screen and (max-width: 768px) {
    padding-bottom: 0 !important;
  }
`;

const LinkColumn = styled(Column)`
  @media screen and (max-width: 768px) {
    padding-top: 0.5rem !important;
  }
`;

const DeleteButton = styled(Icon)`
  cursor: pointer;
  opacity: 0.6;

  &:hover {
    opacity: 0.5;
  }
`;

const CopyIcon = styled(Icon)`
  cursor: pointer;

  &:hover {
    opacity: 0.5;
  }
`;

const MobileHr = styled.hr`
  display: none;
  @media screen and (max-width: 768px) {
    display: block;
  }
`;

const LinkBlock = styled.div`
  display: flex;
  justify-content: space-between;
`;

const getCodeLink = (code: AmbassadorCode, language: Language) => {
  const protocol = window.location.protocol;
  const host = window.location.host;
  const routeParam = getLanguageRouteParam(language);

  return `${protocol}//${host}${routeParam}/?ac=${code.code}`;
};

const AmbassadorCodesList: React.FC = () => {
  const ambassadorCodes = useSelector(getAmbassadorCodes);
  const translate = useTranslate();

  const {
    setCodeToDelete,
    deleteAmbassadorCodeModal,
  } = useDeleteAmbassadorCodeModal();
  const language = useContext(TranslationContext);
  const codesList =
    ambassadorCodes &&
    ambassadorCodes.map((code, key) => {
      const link = getCodeLink(code, language);

      return (
        <div key={key}>
          <Columns>
            <CodeColumn span={3}>
              <Text>{code.code}</Text>
            </CodeColumn>
            <LinkColumn span={7}>
              <LinkBlock>
                <Text>
                  {translate(translations.trackingLink)}
                  <br />
                  <a target="_blank" rel="noopener noreferrer" href={link}>
                    {link}
                  </a>
                </Text>
              </LinkBlock>
            </LinkColumn>
            <IconsColumn span={2}>
              <CopyIcon
                title={translate(translations.copyIcon)}
                type="Copy"
                onClick={() => {
                  copy(link);
                }}
                size={1.5}
              />
              <DeleteButton
                title={translate(translations.deleteCodeButton)}
                type="Trash"
                onClick={() => {
                  setCodeToDelete(code);
                }}
                size={1.5}
              />
            </IconsColumn>
          </Columns>
          <MobileHr />
        </div>
      );
    });

  const noCodesList = (
    <div style={{ color: "red" }}>{translate(translations.emptyCodesList)}</div>
  );

  return (
    <Box mt={10} mb={10}>
      <FormSimpleItem title={translate(translations.subTitle)}>
        {(ambassadorCodes && ambassadorCodes.length > 0 && codesList) ||
          noCodesList}
      </FormSimpleItem>
      {deleteAmbassadorCodeModal}
    </Box>
  );
};

export default AmbassadorCodesList;
