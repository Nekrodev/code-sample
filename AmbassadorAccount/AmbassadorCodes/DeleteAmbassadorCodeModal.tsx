import React, { useCallback, useState } from "react";
import { useDispatch } from "react-redux";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import Modal from "../../../../common/components/Modal";
import { Box } from "../../../../common/components/Layout/Box";
import { Button } from "../../../../common/components/Button";
import { LargeHeading } from "../../../../common/components/TextElements";
import { deleteAmbassadorCode } from "../../../services/ambassadorCodes";
import { removeAmbassadorCode } from "../../../actions/ambassadorCodes";
import { AmbassadorCode } from "../../../types/AmbassadorCode";
import translations from "./translations";

export const useDeleteAmbassadorCodeModal = () => {
  const dispatch = useDispatch();
  const translate = useTranslate();
  const [isDisabled, setIsDisabled] = useState(false);
  const [codeToDelete, setCodeToDelete] = useState<AmbassadorCode | null>(null);

  const onCloseDeleteModal = useCallback(() => {
    setCodeToDelete(null);
  }, []);

  const onClickDeleteCode = useCallback(() => {
    const code = codeToDelete as AmbassadorCode;
    setIsDisabled(true);
    deleteAmbassadorCode(code.id)
      .then(() => {
        dispatch(
          removeAmbassadorCode({
            id: code.id,
          })
        );
      })
      .finally(() => {
        setCodeToDelete(null);
        setIsDisabled(false);
      });
  }, [codeToDelete, dispatch]);

  const deleteAmbassadorCodeModal = (
    <Modal id="delete-code" open={!!codeToDelete} onClose={onCloseDeleteModal}>
      <Box>
        <LargeHeading>{translate(translations.deleteCodeTitle)}</LargeHeading>
        <div>
          {translate(translations.deleteCodeText, [
            codeToDelete && codeToDelete.code,
          ])}
        </div>
      </Box>
      <Box flex={true} direction="row" justify="between" pb={4} pt={5}>
        <Button variation="secondary" onClick={onCloseDeleteModal}>
          {translate(translations.deleteCodeAbort)}
        </Button>
        <Button
          variation="red"
          disabled={isDisabled}
          onClick={onClickDeleteCode}
        >
          {translate(translations.deleteCodeButton)}
        </Button>
      </Box>
    </Modal>
  );

  return {
    setCodeToDelete,
    deleteAmbassadorCodeModal,
  };
};
