import React, { useCallback, useState, useMemo } from "react";
import { Box } from "../../../../common/components/Layout/Box";
import { Input } from "../../../../common/components/Input";
import { SaveButton } from "../../../../passenger/menuItems/ClaimDetail";
import FormSimpleItem from "../../../../common/components/Form/SimpleItem";
import {
  withErrorValidation,
  useFormErrors,
} from "../../../../common/containers/ErrorsProvider";
import { Column, Columns } from "../../../../common/components/Layout/Col";
import translations from "./translations";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import {
  EmptyValidator,
  RegexValidator,
  customValidator,
} from "../../../../common/utilities/validators";
import { SetShowsErrorsProps } from "../../../../common/types/setShowErrorsProps";
import { AmbassadorCodeRegex } from "../../../constants/valiadation";
import { createAmbassadorCode } from "../../../services/ambassadorCodes";
import { useDispatch } from "react-redux";
import { addAmbassadorCode } from "../../../actions/ambassadorCodes";

const baseCodeValidators = [
  EmptyValidator(translations.nonFilledCreateField),
  RegexValidator(translations.errorInvalidCode, AmbassadorCodeRegex),
];

const NewAmbassadorCode: React.FC<
  SetShowsErrorsProps & { setShowsErrors: (value: boolean) => void }
> = (props) => {
  const { setShowsErrors } = props;
  const { hasErrors } = useFormErrors();
  const [code, setCode] = useState("");
  const [isDisabled, setIsDisabled] = useState(false);
  const [codeError, setCodeError] = useState(false);
  const translate = useTranslate();
  const dispatch = useDispatch();

  const onSubmit = useCallback(() => {
    if (hasErrors) {
      setShowsErrors(true);

      return;
    }
    setIsDisabled(true);
    createAmbassadorCode(code)
      .then((value) => {
        dispatch(
          addAmbassadorCode({
            code: {
              code: value.code,
              id: value.id,
            },
          })
        );
        setCode("");
      })
      .catch(() => {
        setCodeError(true);
      })
      .finally(() => {
        setIsDisabled(false);
      });
  }, [code, setShowsErrors, hasErrors, dispatch]);

  const onChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setCode(event.target.value);
      setShowsErrors(false);
      setCodeError(false);
    },
    [setShowsErrors]
  );

  const codeValidators = useMemo(() => {
    if (codeError) {
      setShowsErrors(true);

      return [
        ...baseCodeValidators,
        customValidator(translations.codeExist, true),
      ];
    }

    return baseCodeValidators;
  }, [codeError, setShowsErrors]);

  return (
    <Box>
      <FormSimpleItem title={translate(translations.creationSubtitle)}>
        <Columns>
          <Column span={6}>
            <Input
              value={code}
              placeholder={translate(translations.creationPlaceholder)}
              validators={codeValidators}
              onChange={onChange}
              trimWhiteSpacesOnBlur={true}
            />
          </Column>
          <Column span={6}>
            <SaveButton onClick={onSubmit} float="left" disabled={isDisabled}>
              {translate(translations.createCode)}
            </SaveButton>
          </Column>
        </Columns>
      </FormSimpleItem>
    </Box>
  );
};

export default withErrorValidation(NewAmbassadorCode);
