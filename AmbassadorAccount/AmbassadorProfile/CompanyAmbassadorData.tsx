import React from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import { RelativeBox } from "../../../../common/components/Layout/Box";
import EditForm from "./EditForm";
import translations from "./translations";
import { Columns } from "../../../../common/components/Layout/Col";
import { useSelector } from "react-redux";
import { getCompanyData } from "../../../selectors/profile";
import { modalKeys } from "../../../constants/modals";
import { truncateString } from "../../../../common/helpers";

interface CompanyAmbassadorDataProps {
  openModal: (title: string, modalType: string) => void;
}

const CompanyAmbassadorData: React.FC<CompanyAmbassadorDataProps> = ({
  openModal,
}) => {
  const translate = useTranslate();
  const ambassadorData = useSelector(getCompanyData);

  return (
    <RelativeBox>
      <Columns>
        <EditForm
          onClick={() =>
            openModal(
              translate(translations.CompanyAmbassadorModalTitle),
              modalKeys.companyAmbassador
            )
          }
          title={translate(translations.CompanyTitle)}
        >
          {ambassadorData && (
            <div>
              <div>{truncateString(ambassadorData.companyName)}</div>
              <div>
                {truncateString(ambassadorData.firstName)} {truncateString(ambassadorData.lastName)}
              </div>
              <div>{ambassadorData.phone}</div>
              <div>{ambassadorData.email}</div>
              <div>{ambassadorData.website}</div>
              <div>{ambassadorData.taxId}</div>
            </div>
          )}
        </EditForm>
      </Columns>
    </RelativeBox>
  );
};

export default CompanyAmbassadorData;
