import React, { useCallback } from "react";
import AddressEdit from "../../../../../common/components/EditModalContent/Address";
import { SetShowsErrorsProps } from "../../../../../common/types/setShowErrorsProps";
import { withErrorValidation } from "../../../../../common/containers/ErrorsProvider";
import { useDispatch, useSelector } from "react-redux";
import {
  getAmbassadorType,
  getSavingStatus,
  getAmbassadorBaseData,
} from "../../../../selectors/profile";
import { ambassadorTypeOptions } from "../../../../constants/mainData";
import { saveProfileData } from "../../../../actions/data";
import { ambassadorProfile as refKeys } from "../../../../../common/constants/refKeys";

const Address: React.FC<SetShowsErrorsProps & { onClose: () => void }> = ({
  setShowsErrors,
  onClose,
}) => {
  const baseData = useSelector(getAmbassadorBaseData);
  const dispatch = useDispatch();
  const ambassadorType = useSelector(getAmbassadorType);
  const savingStatus = useSelector(getSavingStatus);
  const saveDataCallback = useCallback(
    (street?: string, zipCity?: string, countryId?: string) => {
      const profileData = {
        ambassadorType: ambassadorType,
        company:
          ambassadorType === ambassadorTypeOptions.Company
            ? { street, zipCity, countryId }
            : null,
        privateAmbassador:
          ambassadorType === ambassadorTypeOptions.PrivateAmbassador
            ? { street, zipCity, countryId }
            : null,
        payment: null,
        donation: null,
      };
      dispatch(saveProfileData(profileData));
    },
    [dispatch, ambassadorType]
  );

  return (
    <AddressEdit
      street={(baseData && baseData.street) || ""}
      zipCity={(baseData && baseData.zipCity) || ""}
      countryId={(baseData && baseData.countryId) || ""}
      setShowsErrors={setShowsErrors}
      onClose={onClose}
      refKeys={refKeys}
      saveDataCallback={saveDataCallback}
      savingStatus={savingStatus}
    />
  );
};

export default withErrorValidation(Address);
