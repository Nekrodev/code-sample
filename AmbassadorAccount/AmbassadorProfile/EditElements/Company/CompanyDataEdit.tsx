import React, { useCallback, useState } from "react";
import translations from "../translations";
import {
  useFormErrors,
  withErrorValidation,
} from "../../../../../../common/containers/ErrorsProvider";
import {
  EmptyValidator,
  RegexValidator,
} from "../../../../../../common/utilities/validators";
import { useTranslate } from "../../../../../../common/containers/TranslationProvider";
import { Box } from "../../../../../../common/components/Layout/Box";
import { Columns } from "../../../../../../common/components/Layout/Col";
import FormItem from "../../../../../../common/components/Form/Item";
import { Input } from "../../../../../../common/components/Input";
import { Button } from "../../../../../../common/components/Button";
import {
  PhoneRegex,
  UrlRegex,
  CompanyTaxIdValueAddedRegex,
} from "../../../../../../common/constants/validation";
import { companyProfile as refKeys } from "../../../../../../common/constants/refKeys";
import { useSelector } from "react-redux";
import { getSavingStatus } from "../../../../../selectors/profile";
import Spinner from "../../../../../../common/components/Spinner";
import ErrorMessage from "../../../../../../common/components/ErrorMessage";

interface CompanyDataEditProps {
  firstName: string;
  lastName: string;
  email: string;
  website?: string;
  companyName: string;
  taxID?: string;
  phone?: string;
  handleSubmit: (
    firstName: string,
    lastName: string,
    phone: string,
    website: string,
    companyName: string,
    taxId: string
  ) => void;
  onClose: () => void;
}

const validators = {
  firstName: [EmptyValidator(translations.emptyFirstNameContactPerson)],
  lastName: [EmptyValidator(translations.emptyLastNameContactPerson)],
  phone: [RegexValidator(translations.phoneInvalid, PhoneRegex)],
  companyName: [EmptyValidator(translations.emptyCompany)],
  website: [RegexValidator(translations.websiteInvalid, UrlRegex)],
  taxId: [
    RegexValidator(translations.taxIdInvalid, CompanyTaxIdValueAddedRegex),
  ],
};

const CompanyDataEdit: React.FC<
  CompanyDataEditProps & { setShowsErrors: (value: boolean) => void }
> = (props) => {
  const { setShowsErrors, onClose } = props;
  const { hasErrors } = useFormErrors();
  const savingStatus = useSelector(getSavingStatus);
  const [firstName, setFirstName] = useState(props.firstName);
  const [lastName, setLastName] = useState(props.lastName);
  const [phone, setPhone] = useState(props.phone || "");
  const [companyName, setCompanyName] = useState(props.companyName);
  const [website, setWebsite] = useState(props.website || "");
  const [taxId, setTaxId] = useState(props.taxID || "");
  const translate = useTranslate();

  const onSubmit = useCallback(() => {
    if (hasErrors) {
      setShowsErrors(true);

      return;
    }
    props.handleSubmit(firstName, lastName, phone, website, companyName, taxId);
    setShowsErrors(false);
  }, [
    setShowsErrors,
    firstName,
    lastName,
    hasErrors,
    phone,
    props,
    website,
    companyName,
    taxId,
  ]);

  return (
    <Spinner isSpin={savingStatus.isSaving}>
      <ErrorMessage error={savingStatus.error} />
      <Box>
        <Columns>
          <FormItem
            type="left"
            title={translate(translations.companyName)}
            required
          >
            <Input
              value={companyName || ""}
              placeholder={translate(translations.companyNamePlaceholder)}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setCompanyName(e.target.value)
              }
              validators={validators.companyName}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.companyName}
              nextRefKey={refKeys.firstName}
            />
          </FormItem>
        </Columns>
        <Columns>
          <FormItem
            type="left"
            title={translate(translations.contactPersonFirstName)}
            required
          >
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setFirstName(e.target.value)
              }
              value={firstName}
              placeholder={translate(
                translations.contactPersonFirstNamePlaceholder
              )}
              validators={validators.firstName}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.firstName}
              nextRefKey={refKeys.lastName}
            />
          </FormItem>
          <FormItem
            type="right"
            title={translate(translations.contactPersonLastName)}
            required
          >
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setLastName(e.target.value)
              }
              value={lastName}
              placeholder={translate(
                translations.contactPersonLastNamePlaceholder
              )}
              validators={validators.lastName}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.lastName}
              nextRefKey={refKeys.phone}
            />
          </FormItem>
        </Columns>
        <Columns>
          <FormItem type="left" title={translate(translations.email)}>
            <Input disabled value={props.email || ""} />
          </FormItem>
          <FormItem type="left" title={translate(translations.phoneTitle)}>
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setPhone(e.target.value)
              }
              value={phone || ""}
              placeholder={translate(translations.phoneNumberPlaceholder)}
              validators={validators.phone}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.phone}
              nextRefKey={refKeys.website}
            />
          </FormItem>
        </Columns>
        <Columns>
          <FormItem type="left" title={translate(translations.website)}>
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setWebsite(e.target.value)
              }
              value={website}
              placeholder={translate(translations.websitePlaceholder)}
              validators={validators.website}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.website}
              nextRefKey={refKeys.taxId}
            />
          </FormItem>
          <FormItem type="right" title={translate(translations.taxId)}>
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setTaxId(e.target.value);
                setShowsErrors(false);
              }}
              value={taxId}
              placeholder={translate(translations.taxIdPlaceholder)}
              validators={validators.taxId}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.taxId}
            />
          </FormItem>
        </Columns>
        <Box flex direction="row" justify="between">
          <Button variation="secondary" onClick={onClose}>
            {translate(translations.cancel)}
          </Button>
          <Button onClick={onSubmit}>{translate(translations.save)}</Button>
        </Box>
      </Box>
    </Spinner>
  );
};

export default withErrorValidation(CompanyDataEdit);
