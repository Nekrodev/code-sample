import React, { useCallback } from "react";
import { saveProfileData } from "../../../../../actions/data";
import { useDispatch, useSelector } from "react-redux";
import {
  getAmbassadorType,
  getCompanyData,
} from "../../../../../selectors/profile";
import CompanyDataEdit from "./CompanyDataEdit";

const Company: React.FC<{ onClose: () => void }> = ({ onClose }) => {
  const companyData = useSelector(getCompanyData);
  const dispatch = useDispatch();
  const ambassadorType = useSelector(getAmbassadorType);
  const saveDataCallback = useCallback(
    (
      firstName: string,
      lastName: string,
      phone: string,
      website: string,
      companyName: string,
      taxId: string
    ) => {
      const profileData = {
        ambassadorType: ambassadorType,
        company: {
          lastName,
          firstName,
          phone,
          website,
          companyName,
          taxId,
        },
        privateAmbassador: null,
        payment: null,
        donation: null,
      };
      dispatch(saveProfileData(profileData));
    },
    [dispatch, ambassadorType]
  );

  return (
    <CompanyDataEdit
      email={(companyData && companyData.email) || ""}
      phone={(companyData && companyData.phone) || ""}
      firstName={(companyData && companyData.firstName) || ""}
      lastName={(companyData && companyData.lastName) || ""}
      website={(companyData && companyData.website) || ""}
      taxID={(companyData && companyData.taxId) || ""}
      companyName={(companyData && companyData.companyName) || ""}
      handleSubmit={saveDataCallback}
      onClose={onClose}
    />
  );
};

export default Company;
