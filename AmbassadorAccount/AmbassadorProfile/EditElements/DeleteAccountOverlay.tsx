import React, { useCallback } from "react";
import translations from "./translations";
import { Box } from "../../../../../common/components/Layout/Box";
import { Button } from "../../../../../common/components/Button";
import { useTranslate } from "../../../../../common/containers/TranslationProvider";
import { deleteAccount } from "../../../../../common/services/Delete";
import { useNavigateWithReload } from "../../../../../routing/useNavigate";
import { ambassadorLogoutParam } from "../../../../../common/constants/appConfig";
import { store as loginStore } from "../../../../../login/store/login-store";
import { accountDeleted } from "../../../../../login/actions/delete";
import { baseRouteNames } from "../../../../../routing/Routes";

interface DeleteAccountOverlayProps {
  onClose: () => void;
}

const DeleteAccountOverlay: React.FC<DeleteAccountOverlayProps> = (props) => {
  const navigate = useNavigateWithReload();
  const handleDelete = useCallback(() => {
    deleteAccount()
      .then(() => {
        loginStore.dispatch(accountDeleted());
        setTimeout(() => {
          navigate(baseRouteNames.home, `?delete=${ambassadorLogoutParam}`);
        }, 500);
      })
      .finally(() => {
        props.onClose();
      });
  }, [props, navigate]);

  const translate = useTranslate();

  return (
    <Box>
      <Box my={2}>{translate(translations.deleteText)}</Box>
      <Box flex direction="row" justify="between">
        <Button variation="secondary" onClick={props.onClose}>
          {translate(translations.abort)}
        </Button>
        <Button variation="red" onClick={handleDelete}>
          {translate(translations.deleteProfile)}
        </Button>
      </Box>
    </Box>
  );
};

export default DeleteAccountOverlay;
