import React, { useCallback, useState } from "react";
import { SetShowsErrorsProps } from "../../../../../common/types/setShowErrorsProps";
import {
  withErrorValidation,
  useFormErrors,
} from "../../../../../common/containers/ErrorsProvider";
import { useDispatch, useSelector } from "react-redux";
import {
  getAmbassadorType,
  getPaymentData,
  getSavingStatus,
} from "../../../../selectors/profile";
import { saveProfileData } from "../../../../actions/data";
import PaymentEdit from "../../../common/PaymentEdit";
import { Button } from "../../../../../common/components/Button";
import { Box } from "../../../../../common/components/Layout/Box";
import { useTranslate } from "../../../../../common/containers/TranslationProvider";
import translations from "./translations";
import { accountTypes, AccountType } from "../../../../types/AccountType";
import { Payment } from "../../../../types/reduxInterfaces";
import Spinner from "../../../../../common/components/Spinner";
import ErrorMessage from "../../../../../common/components/ErrorMessage";
import { ambassadorProfile as refKeys } from "../../../../../common/constants/refKeys";

const getAccount = (paymentData: Payment) => {
  if (paymentData.accountType === accountTypes.BankAccount) {
    return paymentData.bankAccount;
  }
  if (paymentData.accountType === accountTypes.Paypal) {
    return paymentData.paypalEmail;
  }

  return "";
};

const PaymentSettings: React.FC<
  SetShowsErrorsProps & { onClose: () => void }
> = ({ setShowsErrors, onClose }) => {
  const paymentData = useSelector(getPaymentData);
  const { hasErrors } = useFormErrors();
  const translate = useTranslate();
  const savingStatus = useSelector(getSavingStatus);
  const [accountType, setAccountType] = useState(paymentData.accountType);
  const [account, setAccount] = useState(getAccount(paymentData));
  const dispatch = useDispatch();
  const ambassadorType = useSelector(getAmbassadorType);
  const saveDataCallback = useCallback(() => {
    if (hasErrors) {
      setShowsErrors(true);

      return;
    }
    const profileData = {
      ambassadorType: ambassadorType,
      company: null,
      privateAmbassador: null,
      payment: {
        accountType: accountType,
        bankAccount: accountType === accountTypes.BankAccount ? account : "",
        paypalEmail: accountType === accountTypes.Paypal ? account : "",
      },
      donation: null,
    };
    dispatch(saveProfileData(profileData));
  }, [
    dispatch,
    ambassadorType,
    accountType,
    account,
    hasErrors,
    setShowsErrors,
  ]);

  return (
    <Spinner isSpin={savingStatus.isSaving}>
      <ErrorMessage error={savingStatus.error}/>
      <Box mb={3}>
        <PaymentEdit
          account={account}
          setShowsErrors={setShowsErrors}
          updateCallback={(value: string) => {
            setAccount(value);
          }}
          onSelect={(value: AccountType) => {
            setAccountType(value);
            setAccount("");
          }}
          accountType={accountType}
          refKey={refKeys.accountNumber}
        />
      </Box>
      <Box flex direction="row" justify="between">
        <Button variation="secondary" onClick={onClose}>
          {translate(translations.cancel)}
        </Button>
        <Button onClick={saveDataCallback}>
          {translate(translations.save)}
        </Button>
      </Box>
    </Spinner>
  );
};

export default withErrorValidation(PaymentSettings);
