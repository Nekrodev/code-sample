import React, { useCallback, useState } from "react";
import translations from "./translations";
import {
  useFormErrors,
  withErrorValidation,
} from "../../../../../common/containers/ErrorsProvider";
import { EmptyValidator } from "../../../../../common/utilities/validators";
import { useTranslate } from "../../../../../common/containers/TranslationProvider";
import { Box } from "../../../../../common/components/Layout/Box";
import { Columns } from "../../../../../common/components/Layout/Col";
import FormItem from "../../../../../common/components/Form/Item";
import { Input } from "../../../../../common/components/Input";
import { Button } from "../../../../../common/components/Button";
import { useSelector } from "react-redux";
import { getSavingStatus } from "../../../../selectors/profile";
import Spinner from "../../../../../common/components/Spinner";
import ErrorMessage from "../../../../../common/components/ErrorMessage";
import { ambassadorProfile as refKeys } from "../../../../../common/constants/refKeys";

interface PersonalDataEditProps {
  firstName: string;
  lastName: string;
  email: string;
  phone?: string;
  handleSubmit: (firstName: string, lastName: string, phone: string) => void;
  onClose: () => void;
}

const validators = {
  firstName: [EmptyValidator(translations.firstNameEmpty)],
  lastName: [EmptyValidator(translations.lastNameEmpty)],
};

const PersonalDataEdit: React.FC<
  PersonalDataEditProps & { setShowsErrors: (value: boolean) => void }
> = (props) => {
  const { setShowsErrors, handleSubmit, onClose } = props;
  const { hasErrors } = useFormErrors();
  const savingStatus = useSelector(getSavingStatus);
  const [firstName, setFirstName] = useState(props.firstName);
  const [lastName, setLastName] = useState(props.lastName);
  const [phone, setPhone] = useState(props.phone || "");
  const translate = useTranslate();

  const onSubmit = useCallback(() => {
    if (hasErrors) {
      setShowsErrors(true);

      return;
    }

    handleSubmit(firstName, lastName, phone);
    setShowsErrors(false);
  }, [setShowsErrors, firstName, lastName, hasErrors, phone, handleSubmit]);

  return (
    <Spinner isSpin={savingStatus.isSaving}>
      <ErrorMessage error={savingStatus.error} />
      <Box>
        <Columns>
          <FormItem
            type="left"
            title={translate(translations.firstName)}
            required
          >
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setFirstName(e.target.value)
              }
              value={firstName}
              placeholder={translate(translations.firstName)}
              validators={validators.firstName}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.firstName}
              nextRefKey={refKeys.lastName}
            />
          </FormItem>
          <FormItem
            type="right"
            title={translate(translations.lastName)}
            required
          >
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setLastName(e.target.value)
              }
              value={lastName}
              placeholder={translate(translations.lastNamePlaceholder)}
              validators={validators.lastName}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.lastName}
              nextRefKey={refKeys.phone}
            />
          </FormItem>
        </Columns>
        <Columns>
          <FormItem type="left" title={translate(translations.email)}>
            <Input disabled value={props.email || ""} />
          </FormItem>
          <FormItem type="left" title={translate(translations.phoneTitle)}>
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setPhone(e.target.value)
              }
              value={phone || ""}
              placeholder={translate(translations.phoneNumberPlaceholder)}
              trimWhiteSpacesOnBlur={true}
              refKey={refKeys.phone}
            />
          </FormItem>
        </Columns>
        <Box flex direction="row" justify="between">
          <Button variation="secondary" onClick={onClose}>
            {translate(translations.cancel)}
          </Button>
          <Button onClick={onSubmit}>{translate(translations.save)}</Button>
        </Box>
      </Box>
    </Spinner>
  );
};

export default withErrorValidation(PersonalDataEdit);
