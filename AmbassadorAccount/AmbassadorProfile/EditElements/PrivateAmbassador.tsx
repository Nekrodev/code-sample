import React, { useCallback } from "react";
import PersonalDataEdit from "./PersonalDataEdit";
import { saveProfileData } from "../../../../actions/data";
import { useDispatch, useSelector } from "react-redux";
import {
  getAmbassadorType,
  getAmbassadorBaseData,
} from "../../../../selectors/profile";

const PrivateAmbassador: React.FC<{ onClose: () => void }> = ({ onClose }) => {
  const baseData = useSelector(getAmbassadorBaseData);
  const dispatch = useDispatch();
  const ambassadorType = useSelector(getAmbassadorType);
  const saveDataCallback = useCallback(
    (firstName: string, lastName: string, phone: string) => {
      const profileData = {
        ambassadorType: ambassadorType,
        company: null,
        privateAmbassador: { firstName, lastName, phone },
        payment: null,
        donation: null,
      };
      dispatch(saveProfileData(profileData));
    },
    [dispatch, ambassadorType]
  );

  return (
    <PersonalDataEdit
      email={(baseData && baseData.email) || ""}
      phone={(baseData && baseData.phone) || ""}
      firstName={(baseData && baseData.firstName) || ""}
      lastName={(baseData && baseData.lastName) || ""}
      handleSubmit={saveDataCallback}
      onClose={onClose}
    />
  );
};

export default PrivateAmbassador;
