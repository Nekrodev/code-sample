import React from "react";
import { Column } from "../../../../../common/components/Layout/Col";
import { Box } from "../../../../../common/components/Layout/Box";
import { Heading } from "../../../../../common/components/TextElements";
import { useTranslate } from "../../../../../common/containers/TranslationProvider";
import { EditButton } from "../../../../../common/components/Button";

const EditTranslation = {
  de: "Bearbeiten",
  en: "Edit",
};

interface EditFormProps {
  onClick: () => void;
  title?: string;
}

const EditForm: React.FC<EditFormProps> = ({ onClick, title, children }) => {
  const translate = useTranslate();

  return (
    <Column>
      {title && (
        <Box my={3}>
          <Heading>{title}</Heading>
        </Box>
      )}
      <Box>{children}</Box>
      <EditButton variation="secondary" onClick={onClick}>
        {translate(EditTranslation)}
      </EditButton>
    </Column>
  );
};

export default EditForm;
