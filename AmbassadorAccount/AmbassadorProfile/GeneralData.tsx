import React from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import { Box, RelativeBox } from "../../../../common/components/Layout/Box";
import EditForm from "./EditForm";
import translations from "./translations";
import styled from "styled-components";
import { Columns } from "../../../../common/components/Layout/Col";
import { useSelector } from "react-redux";
import { Gray1, Red } from "../../../../common/constants/colors";
import {
  getAmbassadorBaseData,
  getPaymentData,
} from "../../../selectors/profile";
import { accountTypes } from "../../../types/AccountType";
import { modalKeys } from "../../../constants/modals";

const DeleteLink = styled.span`
  cursor: pointer;
  color: ${Gray1};
  margin: 0.5rem 0;
  text-decoration: underline;
`;

const RedText = styled.div`
  color: ${Red};
`;

interface GeneralDataProps {
  openModal: (title: string, modalType: string) => void;
}

const GeneralData: React.FC<GeneralDataProps> = ({ openModal }) => {
  const translate = useTranslate();
  const ambassadorData = useSelector(getAmbassadorBaseData);
  const paymentData = useSelector(getPaymentData);

  if (!ambassadorData) {
    return null;
  }

  return (
    <>
      <hr />

      <RelativeBox>
        <Columns>
          <EditForm
            onClick={() =>
              openModal(
                translate(translations.AddressModalTitle),
                modalKeys.address
              )
            }
            title={translate(translations.AddressTitle)}
          >
            <div>{ambassadorData.street}</div>
            <div>{ambassadorData.zipCity}</div>
            <div>{ambassadorData.countryId}</div>
          </EditForm>
        </Columns>
      </RelativeBox>

      <hr />

      <RelativeBox>
        <Columns>
          <EditForm
            onClick={() => {
              openModal(
                translate(translations.PasswordModalTitle),
                modalKeys.password
              );
            }}
            title={translate(translations.PasswordTitle)}
          >
            ********
          </EditForm>
        </Columns>
      </RelativeBox>

      <hr />

      <RelativeBox>
        <Columns>
          <EditForm
            onClick={() =>
              openModal(
                translate(translations.PaymentModalTitle),
                modalKeys.payment
              )
            }
            title={translate(translations.PaymentTitle)}
          >
            {paymentData.accountType === accountTypes.none && (
              <RedText>{translate(translations.PleaseEnter)}</RedText>
            )}
            {paymentData.accountType === accountTypes.Paypal && (
              <div>
                {translate(translations.Paypal)}
                {": "}
                {paymentData.paypalEmail}
              </div>
            )}
            {paymentData.accountType === accountTypes.BankAccount && (
              <div>
                {translate(translations.Bank)}
                {": "}
                {paymentData.bankAccount}
              </div>
            )}
          </EditForm>
        </Columns>
      </RelativeBox>

      <hr />
      <Box>
        <DeleteLink
          onClick={() => {
            openModal(translate(translations.DeleteProfile), modalKeys.delete);
          }}
        >
          {translate(translations.DeleteProfile)}
        </DeleteLink>
      </Box>
    </>
  );
};

export default GeneralData;
