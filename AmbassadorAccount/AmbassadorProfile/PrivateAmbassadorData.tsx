import React from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import { RelativeBox } from "../../../../common/components/Layout/Box";
import EditForm from "./EditForm";
import translations from "./translations";
import { Columns } from "../../../../common/components/Layout/Col";
import { useSelector } from "react-redux";
import { getAmbassadorBaseData } from "../../../selectors/profile";
import { modalKeys } from "../../../constants/modals";
import { truncateString } from "../../../../common/helpers";

interface PrivateAmbassadorDataProps {
  openModal: (title: string, modalType: string) => void;
}

const PrivateAmbassadorData: React.FC<PrivateAmbassadorDataProps> = ({
  openModal,
}) => {
  const translate = useTranslate();
  const ambassadorData = useSelector(getAmbassadorBaseData);

  return (
    <RelativeBox>
      <Columns>
        <EditForm
          onClick={() =>
            openModal(
              translate(translations.PrivateAmbassadorModalTitle),
              modalKeys.privateAmbassador
            )
          }
          title={translate(translations.PrivateAmbassadorTitle)}
        >
          {ambassadorData && (
            <div>
              <div>
                {truncateString(ambassadorData.firstName)} {truncateString(ambassadorData.lastName)}
              </div>
              <div>{ambassadorData.email}</div>
              <div>{ambassadorData.phone}</div>
            </div>
          )}
        </EditForm>
      </Columns>
    </RelativeBox>
  );
};

export default PrivateAmbassadorData;
