import React, { useCallback } from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import { Box } from "../../../../common/components/Layout/Box";
import { InnerBox } from "../../../../common/components/InnerBox";
import { LargeHeading } from "../../../../common/components/TextElements";
import translations from "./translations";
import { ShadowedBox } from "../../../../common/components/ShadowedBox";
import { useSelector } from "react-redux";
import Spinner from "../../../../common/components/Spinner";
import useModal from "../../../../common/hooks/useModal";
import { modalComponents } from "../../../constants/modals";
import GeneralData from "./GeneralData";
import { ambassadorTypeOptions } from "../../../constants/mainData";
import PrivateAmbassadorData from "./PrivateAmbassadorData";
import CompanyAmbassadorData from "./CompanyAmbassadorData";
import {
  getAmbassadorType,
  getAmbassadorBaseData,
  getLoadingStatus,
} from "../../../selectors/profile";
import { PROFILE_DATA_SAVED } from "../../../constants/actions";
import { store } from "../../../store";
import useActionListener from "../../../../common/hooks/useActionListener";

const ProfileScreen: React.FC = () => {
  const modalState = useModal(modalComponents);
  const translate = useTranslate();
  const ambassadorType = useSelector(getAmbassadorType);
  const ambassadorData = useSelector(getAmbassadorBaseData);
  const { isLoading } = useSelector(getLoadingStatus);
  useActionListener(store, PROFILE_DATA_SAVED, () =>
    modalState.setIsOpen(false)
  );

  const openModal = useCallback(
    (title: string, modalType: string) => {
      modalState.setModalType(modalType);
      modalState.setTitle(title);
      modalState.setIsOpen(true);
    },
    [modalState]
  );

  if (!ambassadorData) {
    return <Spinner isSpin={isLoading} />;
  }

  return (
    <Spinner isSpin={isLoading}>
      <Box>
        <InnerBox as={ShadowedBox} padding={2.5}>
          <Box>
            <LargeHeading>{translate(translations.title)}</LargeHeading>
          </Box>
          {ambassadorType === ambassadorTypeOptions.Company && (
            <CompanyAmbassadorData openModal={openModal} />
          )}
          {ambassadorType === ambassadorTypeOptions.PrivateAmbassador && (
            <PrivateAmbassadorData openModal={openModal} />
          )}
          <GeneralData openModal={openModal} />
          {modalState.dynamicModal()}
        </InnerBox>
      </Box>
    </Spinner>
  );
};

export default ProfileScreen;
