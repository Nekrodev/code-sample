import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { getClaimsColumnsVisibility } from "../../../selectors/claims";
import { claimsColumnKeys } from "../../../constants/table";
import { updateClaimsColumnsVisibility } from "../../../actions/claims";
import {
  ClaimsColumnsVisibilityState,
  ColumnVisibilityState,
} from "../../../types/reduxInterfaces";
import EditColumns from "../EditColumns";

const EditClaimsColumns: React.FC<{ onClose: () => void }> = ({ onClose }) => {
  const state = useSelector(getClaimsColumnsVisibility);
  const dispatch = useDispatch();
  const saveChanges = (newState: ColumnVisibilityState) => {
    dispatch(
      updateClaimsColumnsVisibility(newState as ClaimsColumnsVisibilityState)
    );
    onClose();
  };

  return (
    <EditColumns
      onClose={onClose}
      initialState={state}
      saveCallback={saveChanges}
      columnKeys={claimsColumnKeys}
    />
  );
};

export default EditClaimsColumns;
