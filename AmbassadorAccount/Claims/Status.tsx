import React from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import { TranslationUnit } from "../../../../common/types/Translations";
import { Box } from "../../../../common/components/Layout/Box";

interface Phase {
  id: number;
  translation: TranslationUnit;
}

const Status: React.FC<{ phase: Phase }> = ({ phase }) => {
  const translate = useTranslate();

  return (
    <Box flex={true}>
      <Box ml={1}>{translate(phase.translation)}</Box>
    </Box>
  );
};

export default Status;
