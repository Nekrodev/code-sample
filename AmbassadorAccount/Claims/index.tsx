import React, { useCallback, useMemo, useEffect, useContext } from "react";
import {
  useTranslate,
  TranslationContext,
} from "../../../../common/containers/TranslationProvider";
import { useSelector, useDispatch } from "react-redux";
import translations from "./translations";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import {
  setClaimsTableOrder,
  getClaimsData,
  setClaimsPagination,
  updateClaimsFilters,
  clearClaimsFilters,
} from "../../../actions/claims";
import { tableFieldsMapping } from "../../../constants/table";
import Status from "./Status";
import {
  getClaims,
  getClaimsTableSettings,
  getClaimsColumnsVisibility,
  getClaimsLoadingState,
} from "../../../selectors/claims";
import { modalComponents, modalKeys } from "../../../constants/modals";
import useModal from "../../../../common/hooks/useModal";
import TableView, {
  getDefaultColumnOptions,
  getPaginationOptions,
} from "../TableView";
import {
  useFormatPrice,
  useFormatNumber,
} from "../../../../common/hooks/FormatNumbers";
import {
  ColumnDescription,
  TableChangeState,
  TableChangeType,
} from "react-bootstrap-table-next";
import { AmbassadorClaim } from "../../../types/AmbassadorClaim";
import { PaginationOption } from "../../../types/AmbassadorRecommendation";
import { useFormatDate } from "../../../../common/hooks/useFormatDate";

const ClaimsScreen: React.FC = () => {
  const loadingState = useSelector(getClaimsLoadingState);
  const columnsVisibility = useSelector(getClaimsColumnsVisibility);
  const modalState = useModal(modalComponents);
  const formatNumber = useFormatNumber();
  const formatPrice = useFormatPrice();
  const formatDate = useFormatDate();
  const currentLanguage = useContext(TranslationContext);
  const openModal = useCallback(
    (title: string, type: string) => {
      modalState.setModalType(type);
      modalState.setTitle(title);
      modalState.setIsOpen(true);
    },
    [modalState]
  );

  const tableSettings = useSelector(getClaimsTableSettings);
  const {
    page,
    totalSize,
    recordsPerPage,
    orderBy,
    orderDirection,
    filters,
  } = tableSettings;
  const translate = useTranslate();
  const dispatch = useDispatch();
  const claims = useSelector(getClaims);

  const sortingCallback = useCallback(
    (key: string, order: string) => {
      dispatch(setClaimsTableOrder(order, key));
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(
      getClaimsData({
        page,
        recordsPerPage,
        orderBy,
        orderDirection,
        filters,
      })
    );
  }, [page, recordsPerPage, orderBy, orderDirection, dispatch, filters]);

  useEffect(() => {
    return () => {
      dispatch(clearClaimsFilters());
    };
  }, [dispatch]);

  const onTableChange = useCallback(
    (type: TableChangeType, newState: TableChangeState<AmbassadorClaim>) => {
      switch (type) {
        case "filter":
          const filters = Object.keys(newState.filters).map((key) => {
            const value = newState.filters[key].filterVal;

            return { key: tableFieldsMapping[key], value };
          });

          if (filters.length === 0) {
            dispatch(clearClaimsFilters());
          } else {
            dispatch(updateClaimsFilters(filters));
          }
          break;
        case "pagination":
          dispatch(
            setClaimsPagination(
              newState.page,
              newState.sizePerPage as PaginationOption
            )
          );
          break;
        default:
          return;
      }
    },
    [dispatch]
  );

  const columns: ColumnDescription<any, any>[] = useMemo(() => {
    return [
      {
        dataField: "flightDate",
        text: translate(translations.flightDate),
        hidden: !columnsVisibility.flightDate,
        formatter: formatDate,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "reference",
        text: translate(translations.reference),
        hidden: !columnsVisibility.reference,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "ambassadorCode",
        text: translate(translations.ambassadorCode),
        hidden: !columnsVisibility.ambassadorCode,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "passengerName",
        text: translate(translations.passengerName),
        hidden: !columnsVisibility.passengerName,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "route",
        text: translate(translations.flightRoute),
        hidden: !columnsVisibility.flightRoute,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "amount",
        text: translate(translations.amount),
        type: "number",
        hidden: !columnsVisibility.amount,
        formatter: (cell) => formatPrice(formatNumber(cell)),
        style: {
          width: "2.5rem",
        },
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "phase",
        text: translate(translations.status),
        ...getDefaultColumnOptions(sortingCallback),
        hidden: !columnsVisibility.status,
        formatter: (row) => {
          return <Status phase={row} />;
        },
      },
      {
        dataField: "id",
        hidden: true,
        text: "",
      },
    ];
  }, [
    translate,
    columnsVisibility,
    sortingCallback,
    formatPrice,
    formatNumber,
    formatDate,
  ]);

  return (
    <>
      <TableView
        key={currentLanguage}
        handleEditColumnsClick={() => {
          openModal(
            translate(translations.editColumns),
            modalKeys.editClaimsColumns
          );
        }}
        onTableChange={onTableChange}
        data={claims}
        columns={columns}
        title={translate(translations.title)}
        paginationOptions={getPaginationOptions(
          page,
          recordsPerPage,
          totalSize
        )}
        loadingState={loadingState}
        columnVisibility={columnsVisibility}
      />
      {modalState.dynamicModal()}
    </>
  );
};

export default ClaimsScreen;
