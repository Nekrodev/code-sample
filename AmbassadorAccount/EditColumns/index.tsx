import React, { useState } from "react";
import { Box } from "../../../../common/components/Layout/Box";
import translations from "./translations";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import Checkbox from "../../../../common/components/Checkbox";
import { Button } from "../../../../common/components/Button";
import { ColumnVisibilityState } from "../../../types/reduxInterfaces";
import { getEmptyTranslationUnit } from "../../../../common/types/Translations";

export interface EditColumnsProps {
  onClose: () => void;
  initialState: ColumnVisibilityState;
  columnKeys: object;
  saveCallback: (newState: ColumnVisibilityState) => void;
}

const EditColumns: React.FC<EditColumnsProps> = ({
  onClose,
  initialState,
  columnKeys,
  saveCallback,
}) => {
  const [modalState, setModalState] = useState(initialState);
  const translate = useTranslate();
  const handleChange = (key: string, value: boolean) => {
    const newState = {};
    newState[key] = value;
    setModalState({ ...modalState, ...newState });
  };

  return (
    <Box>
      <Box flex={true} direction="column" justify="between">
        {Object.keys(columnKeys).map((key) => (
          <Box mt={1} key={`${key}`}>
            <Checkbox
              value={modalState[key]}
              onChange={(value: boolean) => handleChange(key, value)}
            >
              {translate(translations[key] || getEmptyTranslationUnit())}
            </Checkbox>
          </Box>
        ))}
      </Box>
      <Box flex={true} direction="row" justify="between" mt={3}>
        <Button variation="secondary" onClick={() => onClose()}>
          {translate(translations.abort)}
        </Button>
        <Button onClick={() => saveCallback(modalState)}>
          {translate(translations.save)}
        </Button>
      </Box>
    </Box>
  );
};

export default EditColumns;
