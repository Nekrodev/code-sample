import React, { useEffect } from "react";
import { useRoute } from "react-router5";
import { baseRouteNames } from "../../../routing/Routes";
import AmbassadorProfile from "./AmbassadorProfile";
import { getAmbassadorCodes as fetchAmbassadorCodes } from "../../services/ambassadorCodes";
import { useDispatch } from "react-redux";
import { getProfileData } from "../../actions/data";
import Recommendations from "./Recommendations";
import AmbassadorCodes from "./AmbassadorCodes/AmbassadorCodes";
import { updateAmbassadorCodes } from "../../actions/ambassadorCodes";
import { AmbassadorCode } from "../../types/AmbassadorCode";
import ClaimsScreen from "./Claims";
import { routeNameMatches } from "../../../routing/helpers";

const MainView: React.FC = () => {
  const { route } = useRoute();
  const routeName = route.name;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProfileData());
  }, [dispatch]);

  useEffect(() => {
    fetchAmbassadorCodes().then((responseData) => {
      const codes =
        responseData &&
        responseData.map((value) => {
          const ambassadorCode: AmbassadorCode = {
            code: value.code,
            id: value.id,
          };

          return ambassadorCode;
        });
      if (codes) {
        dispatch(updateAmbassadorCodes({ codes }));
      }
    });
  }, [dispatch]);

  if (routeNameMatches(routeName, baseRouteNames.ambassadorProfileClaims)) {
    return <ClaimsScreen />;
  }

  if (routeNameMatches(routeName, baseRouteNames.ambassadorProfileData)) {
    return <AmbassadorProfile />;
  }

  if (routeNameMatches(routeName, baseRouteNames.ambassadorProfileRecommendations)) {
    return <Recommendations />;
  }

  if (routeNameMatches(routeName, baseRouteNames.ambassadorProfileCodes)) {
    return <AmbassadorCodes />;
  }

  return null;
};

export default MainView;
