import React, { useMemo } from "react";
import { useTranslate } from "../../../common/containers/TranslationProvider";
import { baseRouteNames } from "../../../routing/Routes";
import Navigation from "../../../common/components/Navigation";
import translations from "./translations";
import { DimError, Red } from "../../../common/constants/colors";
import { useSelector } from "react-redux";
import { getAmbassadorCodes } from "../../selectors/codes";

const useOptions = () => {
  const translate = useTranslate();
  const ambassadorCodes = useSelector(getAmbassadorCodes);
  const hasCodes = !!ambassadorCodes && !!ambassadorCodes.length;
  const items = [
    {
      routeName: baseRouteNames.ambassadorProfileClaims,
      title: translations.Claims,
    },
    {
      routeName: baseRouteNames.ambassadorProfileRecommendations,
      title: translations.Recommendations,
    },
    {
      routeName: baseRouteNames.ambassadorProfileCodes,
      title: translations.Codes,
      color: !hasCodes ? DimError : undefined,
      activeColor: !hasCodes ? Red : undefined,
    },
    {
      routeName: baseRouteNames.ambassadorProfileData,
      title: translations.Profile,
    },
    {
      routeName: baseRouteNames.home,
      title: translations.Logout,
    },
  ];

  return useMemo(
    () =>
      items.map((item) => ({
        routeName: item.routeName,
        activeColor: item.activeColor,
        color: item.color,
        label: translate(item.title),
      })),
    [translate, items]
  );
};

const AmbassadorNavigation: React.FC = () => {
  const options = useOptions();

  return <Navigation items={options} />;
};

export default AmbassadorNavigation;
