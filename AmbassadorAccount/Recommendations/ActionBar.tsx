import React, { useCallback } from "react";
import styled from "styled-components";
import { Gray5 } from "../../../../common/constants/colors";
import { Box } from "../../../../common/components/Layout/Box";
import { Icon } from "../../../../common/components/Icon";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import translations from "./translations";
import { useDispatch } from "react-redux";
import {
  setModalState,
  deleteRecommendationRow,
} from "../../../actions/recommendations";

const InfoBox = styled(Box)`
  position: relative;
  right: 250px;
  top: 101%;
  background-color: white;
  box-shadow: 0 0 10px 1px ${Gray5};
  padding: 1rem;
  z-index: 10;
  min-width: 250px;
  font-size: 1rem;
  font-weight: normal;
`;

const IconHover = styled(Icon)`
  background-position: center;
  cursor: pointer;
  ${InfoBox} {
    display: none;
  }

  &:hover {
    ${InfoBox} {
      display: block;
    }
  }
`;

const ActionBar: React.FC<{
  id: number;
  recommendationId: number;
  email: string;
  openModal: () => void;
}> = ({ id, recommendationId, openModal, email }) => {
  const dispatch = useDispatch();
  const resendRecommendation = useCallback(() => {
    dispatch(setModalState(recommendationId, email));
    openModal();
  }, [openModal, recommendationId, email, dispatch]);
  const translate = useTranslate();

  return (
    <Box flex={true} justify="between" align="center" key={`${id}`}>
      <IconHover type="envelope" size={1.2} onClick={resendRecommendation}>
        <InfoBox>{translate(translations.resendRecommendation)}</InfoBox>
      </IconHover>
      <IconHover
        type="Trash"
        size={1.2}
        onClick={() => {
          dispatch(deleteRecommendationRow(id));
        }}
      >
        <InfoBox>{translate(translations.deleteRecommendation)}</InfoBox>
      </IconHover>
    </Box>
  );
};

export default ActionBar;
