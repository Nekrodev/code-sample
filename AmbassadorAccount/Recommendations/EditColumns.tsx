import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { getRecommendationsColumnsVisibility } from "../../../selectors/recommendations";
import { updateColumnsVisibility } from "../../../actions/recommendations";
import { recommendationsColumnKeys } from "../../../constants/table";
import {
  RecommendationsColumnsVisibilityState,
  ColumnVisibilityState,
} from "../../../types/reduxInterfaces";
import EditColumns from "../EditColumns";

const EditRecommendationColumns: React.FC<{ onClose: () => void }> = ({
  onClose,
}) => {
  const state = useSelector(getRecommendationsColumnsVisibility);
  const dispatch = useDispatch();
  const saveChanges = (newState: ColumnVisibilityState) => {
    dispatch(
      updateColumnsVisibility(newState as RecommendationsColumnsVisibilityState)
    );
    onClose();
  };

  return (
    <EditColumns
      onClose={onClose}
      initialState={state}
      columnKeys={recommendationsColumnKeys}
      saveCallback={saveChanges}
    />
  );
};

export default EditRecommendationColumns;
