import React, { useState, useCallback } from "react";
import { Box } from "../../../../common/components/Layout/Box";
import { Columns } from "../../../../common/components/Layout/Col";
import FormItem from "../../../../common/components/Form/Item";
import translations from "./translations";
import { Input } from "../../../../common/components/Input";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import {
  EmptyValidator,
  RegexValidator,
} from "../../../../common/utilities/validators";
import { EmailRegex } from "../../../../common/constants/validation";
import { Button } from "../../../../common/components/Button";
import { SetShowsErrorsProps } from "../../../../common/types/setShowErrorsProps";
import {
  withErrorValidation,
  useFormErrors,
} from "../../../../common/containers/ErrorsProvider";
import { useSelector, useDispatch } from "react-redux";
import {
  getResendEmailModalState,
  getRecommendationsTableSettings,
} from "../../../selectors/recommendations";
import { resendRecommendation } from "../../../services/recommendation";
import ErrorMessage from "../../../../common/components/ErrorMessage";
import Spinner from "../../../../common/components/Spinner";
import { getRecommendationData } from "../../../actions/recommendations";

const validators = [
  EmptyValidator(translations.emptyEmail),
  RegexValidator(translations.invalidEmail, EmailRegex),
];

interface ResendEmailProps {
  onClose: () => void;
  recommendationId: number;
  prefilledEmail: string;
}

const ResendEmail: React.FC<ResendEmailProps & SetShowsErrorsProps> = ({
  onClose,
  setShowsErrors,
}) => {
  const tableSettings = useSelector(getRecommendationsTableSettings);
  const modalState = useSelector(getResendEmailModalState);
  const { recommendationId } = modalState;
  const { hasErrors } = useFormErrors();
  const [email, setEmail] = useState(modalState.email);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const translate = useTranslate();
  const dispatch = useDispatch();

  const onSubmit = useCallback(() => {
    if (hasErrors) {
      setShowsErrors(true);

      return;
    }
    setIsLoading(true);
    resendRecommendation(recommendationId, email)
      .then(() => {
        onClose();
        dispatch(getRecommendationData(tableSettings));
      })
      .catch((e) => {
        setError(e.message);
      })
      .finally(() => setIsLoading(false));
  }, [
    hasErrors,
    setShowsErrors,
    recommendationId,
    email,
    onClose,
    dispatch,
    tableSettings,
  ]);

  const handleEmailInputChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setEmail(e.target.value);
      setShowsErrors(false);
    },
    [setEmail, setShowsErrors]
  );

  return (
    <Spinner isSpin={isLoading}>
      <ErrorMessage message={error} />
      <Columns>
        <FormItem
          type="left"
          title={translate(translations.email)}
          required={true}
        >
          <Input
            value={email}
            placeholder={translate(translations.emailPlaceholder)}
            onChange={handleEmailInputChange}
            validators={validators}
            trimWhiteSpacesOnBlur={true}
          />
        </FormItem>
      </Columns>
      <Box flex={true} direction="row" justify="between">
        <Button variation="secondary" onClick={() => onClose()}>
          {translate(translations.abort)}
        </Button>
        <Button onClick={onSubmit}>
          {translate(translations.resendRecommendation)}
        </Button>
      </Box>
    </Spinner>
  );
};

export default withErrorValidation(ResendEmail);
