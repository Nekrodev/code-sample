import React from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import translations from "./translations";

const Status: React.FC<{ isSigned: boolean }> = ({ isSigned }) => {
  const translate = useTranslate();

  return isSigned
    ? translate(translations.transformedIntoClaim)
    : translate(translations.pending);
};

export default Status;
