import React, { useCallback, useMemo, useEffect, useContext } from "react";
import {
  useTranslate,
  TranslationContext,
} from "../../../../common/containers/TranslationProvider";
import { useSelector, useDispatch } from "react-redux";
import translations from "./translations";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import {
  setTableOrder,
  getRecommendationData,
  setPagination,
  updateRecommendationsFilters,
  clearRecommendationsFilters,
} from "../../../actions/recommendations";
import { selectFilter } from "react-bootstrap-table2-filter";
import { tableFieldsMapping } from "../../../constants/table";
import Status from "./Status";
import ActionBar from "./ActionBar";
import {
  getRecommendations,
  getRecommendationsTableSettings,
  getRecommendationsColumnsVisibility,
  getRecommendationsLoadingState,
} from "../../../selectors/recommendations";
import { modalComponents, modalKeys } from "../../../constants/modals";
import useModal from "../../../../common/hooks/useModal";
import TableView, {
  getPaginationOptions,
  getDefaultColumnOptions,
} from "../TableView";
import {
  useFormatPrice,
  useFormatNumber,
} from "../../../../common/hooks/FormatNumbers";
import { TableChangeType, TableChangeState } from "react-bootstrap-table-next";
import {
  AmbassadorRecommendation,
  PaginationOption,
} from "../../../types/AmbassadorRecommendation";
import { useFormatDate } from "../../../../common/hooks/useFormatDate";

const normalizeStatusField = (selectedOption: number) => {
  switch (selectedOption) {
    case 1:
      return false;
    case 2:
      return true;
    default:
      return null;
  }
};

const RecommendationScreen: React.FC = () => {
  const loadingState = useSelector(getRecommendationsLoadingState);
  const columnsVisibility = useSelector(getRecommendationsColumnsVisibility);
  const modalState = useModal(modalComponents);
  const formatPrice = useFormatPrice();
  const formatNumber = useFormatNumber();
  const formatDate = useFormatDate();
  const currentLanguage = useContext(TranslationContext);
  const openModal = useCallback(
    (title: string, type: string) => {
      modalState.setModalType(type);
      modalState.setTitle(title);
      modalState.setIsOpen(true);
    },
    [modalState]
  );
  const tableSettings = useSelector(getRecommendationsTableSettings);
  const {
    page,
    totalSize,
    recordsPerPage,
    orderBy,
    orderDirection,
    filters,
  } = tableSettings;
  const translate = useTranslate();
  const dispatch = useDispatch();
  const recommendations = useSelector(getRecommendations);

  const statusOptions = useMemo(
    () => ({
      0: translate(translations.all),
      1: translate(translations.pending),
      2: translate(translations.transformed),
    }),
    [translate]
  );

  const sortingCallback = useCallback(
    (key: string, order: string) => {
      dispatch(setTableOrder(order, key));
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(
      getRecommendationData({
        page,
        recordsPerPage,
        orderBy,
        orderDirection,
        filters,
      })
    );
  }, [page, recordsPerPage, orderBy, orderDirection, dispatch, filters]);

  useEffect(() => {
    return () => {
      dispatch(clearRecommendationsFilters());
    };
  }, [dispatch]);

  const onTableChange = useCallback(
    (
      type: TableChangeType,
      newState: TableChangeState<AmbassadorRecommendation>
    ) => {
      switch (type) {
        case "filter":
          const filtered = Object.keys(newState.filters).filter((key) => {
            const value = newState.filters[key].filterVal;
            if (key === "isSigned" && parseInt(value, 0) === 0) {
              return false;
            }

            return true;
          });
          const filters = filtered.map((key) => {
            const value = newState.filters[key].filterVal;
            if (key === "isSigned") {
              const isSigned = normalizeStatusField(parseInt(value, 0));

              return {
                key: tableFieldsMapping[key],
                value: isSigned,
              };
            }

            return { key: tableFieldsMapping[key], value };
          });

          if (filters.length === 0) {
            dispatch(clearRecommendationsFilters());
          } else {
            dispatch(updateRecommendationsFilters(filters));
          }
          break;
        case "pagination":
          dispatch(
            setPagination(
              newState.page,
              newState.sizePerPage as PaginationOption
            )
          );
          break;
        default:
          return;
      }
    },
    [dispatch]
  );

  const columns = useMemo(() => {
    return [
      {
        dataField: "flightDate",
        text: translate(translations.flightDate),
        hidden: !columnsVisibility.flightDate,
        ...getDefaultColumnOptions(sortingCallback),
        formatter: formatDate,
      },
      {
        dataField: "reference",
        text: translate(translations.reference),
        hidden: !columnsVisibility.reference,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "passengerName",
        text: translate(translations.passengerName),
        hidden: !columnsVisibility.passengerName,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "route",
        text: translate(translations.flightRoute),
        hidden: !columnsVisibility.flightRoute,
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "amount",
        text: translate(translations.amount),
        type: "number",
        hidden: !columnsVisibility.amount,
        formatter: (cell) => formatPrice(formatNumber(cell)),
        style: {
          width: "2.5rem",
        },
        ...getDefaultColumnOptions(sortingCallback),
      },
      {
        dataField: "isSigned",
        text: translate(translations.status),
        search: true,
        classes: "bootstrap-table-cell",
        sort: true,
        hidden: !columnsVisibility.status,
        headerClasses: "bootstrap-table-header",
        onSort: sortingCallback,
        formatter: (row) => {
          return <Status isSigned={row} />;
        },
        filter: selectFilter({
          options: statusOptions,
          withoutEmptyOption: true,
          defaultValue: 0,
          className: "bootstrap-table-search-field",
        }),
      },
      {
        dataField: "id",
        classes: "bootstrap-table-cell",
        hidden: !columnsVisibility.actions,
        headerClasses: "bootstrap-table-header",
        text: translate(translations.actions),
        formatter: (cell, row) => {
          return (
            <ActionBar
              id={cell || -1}
              recommendationId={row.recommendationId}
              openModal={() =>
                openModal(
                  translate(translations.resendRecommendation),
                  modalKeys.resendEmail
                )
              }
              email={row.email}
            />
          );
        },
      },
      {
        dataField: "recommendationId",
        hidden: true,
        text: "",
      },
      {
        dataField: "email",
        hidden: true,
        text: "",
      },
    ];
  }, [
    translate,
    sortingCallback,
    statusOptions,
    openModal,
    columnsVisibility,
    formatPrice,
    formatNumber,
    formatDate,
  ]);

  return (
    <>
      <TableView
        key={currentLanguage}
        handleEditColumnsClick={() => {
          openModal(
            translate(translations.editColumns),
            modalKeys.editRecommendationsColumns
          );
        }}
        onTableChange={onTableChange}
        data={recommendations}
        columns={columns}
        title={translate(translations.title)}
        paginationOptions={getPaginationOptions(
          page,
          recordsPerPage,
          totalSize
        )}
        loadingState={loadingState}
        columnVisibility={columnsVisibility}
      />
      {modalState.dynamicModal()}
    </>
  );
};

export default RecommendationScreen;
