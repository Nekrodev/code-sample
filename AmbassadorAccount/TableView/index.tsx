import React from "react";
import { useTranslate } from "../../../../common/containers/TranslationProvider";
import { Box } from "../../../../common/components/Layout/Box";
import { InnerBox } from "../../../../common/components/InnerBox";
import { LargeHeading } from "../../../../common/components/TextElements";
import { ShadowedBox } from "../../../../common/components/ShadowedBox";
import Spinner from "../../../../common/components/Spinner";
import OutlineButton from "../../../../common/components/SquareButton";
import BootstrapTable, {
  ColumnDescription,
  TableChangeType,
  TableChangeState,
} from "react-bootstrap-table-next";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import paginationFactory from "react-bootstrap-table2-paginator";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import { Button } from "../../../../common/components/Button";
import ErrorMessage from "../../../../common/components/ErrorMessage";
import { baseRouteNames } from "../../../../routing/Routes";
import useNavigate from "../../../../routing/useNavigate";
import translations from "./translations";
import styled from "styled-components";
import { ColumnVisibilityState } from "../../../types/reduxInterfaces";

const TableContainer = styled(InnerBox)`
  width: fit-content;
`;

export const getDefaultColumnOptions = (
  sortingCallback: (key: string, order: string) => void
) => ({
  search: true,
  sort: true,
  onSort: sortingCallback,
  classes: "bootstrap-table-cell",
  headerClasses: "bootstrap-table-header",
  filter: textFilter({
    delay: 1000,
    className: "bootstrap-table-search-field",
    placeholder: "search",
  }),
});

export const getPaginationOptions = (
  page: number,
  recordsPerPage: number,
  totalSize: number
) => {
  return {
    page: page,
    sizePerPage: recordsPerPage,
    totalSize: totalSize,
    paginationSize:
      Math.ceil(totalSize / recordsPerPage) < 5
        ? Math.ceil(totalSize / recordsPerPage)
        : 5,
    pageStartIndex: 1,
    showTotal: false,
    disablePageTitle: true,
    sizePerPageList: [
      {
        text: "20",
        value: 20,
      },
      {
        text: "50",
        value: 50,
      },
      {
        text: "100",
        value: 100,
      },
    ],
  };
};

const remoteOptions = { filter: true, pagination: true };

interface TableViewProps {
  handleEditColumnsClick: () => void;
  onTableChange: (
    type: TableChangeType,
    newState: TableChangeState<any>
  ) => void;
  data: any[];
  columns: ColumnDescription<any, any>[];
  paginationOptions: object;
  title: string;
  loadingState: {
    isLoading: boolean;
    error: string;
  };
  columnVisibility: ColumnVisibilityState;
}

export const TableView: React.FC<TableViewProps> = ({
  handleEditColumnsClick,
  onTableChange,
  data,
  columns,
  paginationOptions,
  loadingState,
  title,
  columnVisibility,
}) => {
  const renderTable = Object.values(columnVisibility).reduce(
    (total, current) => total || current
  );
  const navigate = useNavigate();
  const translate = useTranslate();

  return (
    <>
      <Spinner isSpin={loadingState.isLoading} spinnerType="fixed" />
      <Box flex={true} pb={2} justify="between">
        <OutlineButton
          icon="add"
          text={translate(translations.newClaimButton)}
          onClick={() =>
            navigate(baseRouteNames.compensationCheck, {
              clearState: true,
              isRecommendation: true,
            })
          }
        />
        <Button variation="secondary" onClick={handleEditColumnsClick}>
          {translate(translations.editColumns)}
        </Button>
      </Box>
      <Box mb={4}>
        <TableContainer as={ShadowedBox} padding={2.5}>
          <Box>
            <LargeHeading>{title}</LargeHeading>
          </Box>
          <ErrorMessage message={loadingState.error} />
          {renderTable && (
            <BootstrapTable
              onTableChange={onTableChange}
              remote={remoteOptions}
              bootstrap4={true}
              keyField="id"
              data={data}
              columns={columns}
              filter={filterFactory()}
              pagination={paginationFactory(paginationOptions)}
            />
          )}
        </TableContainer>
      </Box>
    </>
  );
};

export default TableView;
