import React from "react";
import "react-dates/initialize";
import { withTranslations } from "../../../common/containers/TranslationProvider";
import PersistedReduxStore from "../../store";
import { Columns, Column } from "../../../common/components/Layout/Col";
import MainView from "./MainView";
import useLanguageInitialization from "../../../common/hooks/useLanguageInitialization";
import AmbassadorNavigation from "./Navigation";
import PendingNotice from "../PendingNotice";
import ErrorMessage from "../../../common/components/ErrorMessage";
import { useSelector } from "react-redux";
import { getLoadingStatus } from "../../selectors/profile";
import Spinner from "../../../common/components/Spinner";
import styled from "styled-components";
import { routeNameMatches } from "../../../routing/helpers";
import { baseRouteNames } from "../../../routing/Routes";
import { useRoute } from "react-router5";

const AmbassadorContainer = styled.div<{ isTableView: boolean }>`
  overflow-x: ${(props) => (props.isTableView ? "auto" : "hidden")};
  overflow-y: hidden;
  padding: 2rem 0;
`;

const isTableView = (routeName: string) => {
  return (
    routeNameMatches(
      routeName,
      baseRouteNames.ambassadorProfileRecommendations
    ) || routeNameMatches(routeName, baseRouteNames.ambassadorProfileClaims)
  );
};

const AmbassadorProfile: React.FC = () => {
  const { route } = useRoute();
  useLanguageInitialization();
  const loadingStatus = useSelector(getLoadingStatus);

  return (
    <AmbassadorContainer isTableView={isTableView(route.name)}>
      <PendingNotice />
      <Columns additionalClassName="no-margin">
        <Column span={3} className="no-padding-left">
          <AmbassadorNavigation />
        </Column>
        <Column span={9} className="no-padding-right">
          <Spinner isSpin={loadingStatus.isLoading && !isTableView(route.name)}>
            <ErrorMessage error={loadingStatus.error} />
            <MainView />
          </Spinner>
        </Column>
      </Columns>
    </AmbassadorContainer>
  );
};

const WrappedAmbassadorProfile: React.FC = () => {
  return (
    <PersistedReduxStore>
      <AmbassadorProfile />
    </PersistedReduxStore>
  );
};

export default withTranslations(WrappedAmbassadorProfile);
