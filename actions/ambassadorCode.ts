import { UpdateAmbassadorCodeAction } from "./../types/actions/AmbassadorCode";
import { UPDATE_AMBASSADOR_CODE } from "../constants/actions";

export const updateAmbassadorCode = (
  code: string
): UpdateAmbassadorCodeAction => ({
  type: UPDATE_AMBASSADOR_CODE,
  payload: { code },
});
