import {
  UPDATE_AMBASSADOR_CODES,
  ADD_AMBASSADOR_CODE,
  REMOVE_AMBASSADOR_CODE,
} from "../constants/actions";
import {
  UpdateCodesAction,
  UpdateCodesActionPayload,
  AddCodeAction,
  AddCodeActionPayload,
  RemoveCodeAction,
  RemoveCodeActionPayload,
} from "../types/actions/AmbassadorCodes";

export const updateAmbassadorCodes = (
  payload: UpdateCodesActionPayload
): UpdateCodesAction => ({
  type: UPDATE_AMBASSADOR_CODES,
  payload,
});

export const addAmbassadorCode = (
  payload: AddCodeActionPayload
): AddCodeAction => ({
  type: ADD_AMBASSADOR_CODE,
  payload,
});

export const removeAmbassadorCode = (
  payload: RemoveCodeActionPayload
): RemoveCodeAction => ({
  type: REMOVE_AMBASSADOR_CODE,
  payload,
});
