import {
  GetClaimsRequestParams,
  ClaimsColumnsVisibilityState,
} from "../types/reduxInterfaces";
import { AmbassadorClaim } from "../types/AmbassadorClaim";
import {
  GOT_CLAIMS_DATA,
  GET_CLAIMS_DATA,
  GET_CLAIMS_DATA_FAILED,
  SET_CLAIMS_TABLE_ORDER,
  SET_CLAIMS_TABLE_PAGINATION,
  UPDATE_CLAIMS_FILTERS,
  UPDATE_CLAIMS_COLUMNS_VISIBILITY,
  CLEAR_CLAIMS_FILTERS,
} from "../constants/actions";
import {
  GetClaimsDataFailedAction,
  GetClaimsDataAction,
  GotClaimsDataAction,
} from "../types/actions/AmbassadorClaims";
import {
  SetClaimsOrderAction,
  SetClaimsPaginationAction,
  UpdateClaimsFiltersAction,
  UpdateClaimsColumnsVisibilityAction,
  ClearClaimsFiltersAction,
} from "../types/actions/claimsTableSettings";
import { PaginationOption } from "../types/AmbassadorRecommendation";

export const gotClaimsData = (
  claims: AmbassadorClaim[],
  totalSize: number
): GotClaimsDataAction => ({
  type: GOT_CLAIMS_DATA,
  payload: { claims, totalSize },
});

export const getClaimsData = (
  requestParams: GetClaimsRequestParams
): GetClaimsDataAction => ({
  type: GET_CLAIMS_DATA,
  payload: { requestParams },
});

export const getClaimsDataFailed = (
  error: Error
): GetClaimsDataFailedAction => ({
  type: GET_CLAIMS_DATA_FAILED,
  payload: { error },
});

export const setClaimsTableOrder = (
  orderDirection: string,
  orderBy: string
): SetClaimsOrderAction => ({
  type: SET_CLAIMS_TABLE_ORDER,
  payload: { orderBy, orderDirection },
});

export const setClaimsPagination = (
  page: number,
  recordsPerPage: PaginationOption
): SetClaimsPaginationAction => ({
  type: SET_CLAIMS_TABLE_PAGINATION,
  payload: { page, recordsPerPage },
});

export const updateClaimsFilters = (
  filters: { key: string; value: string }[]
): UpdateClaimsFiltersAction => ({
  type: UPDATE_CLAIMS_FILTERS,
  payload: { filters },
});

export const clearClaimsFilters = (): ClearClaimsFiltersAction => ({
  type: CLEAR_CLAIMS_FILTERS,
});

export const updateClaimsColumnsVisibility = (
  newState: ClaimsColumnsVisibilityState
): UpdateClaimsColumnsVisibilityAction => ({
  type: UPDATE_CLAIMS_COLUMNS_VISIBILITY,
  payload: { newState },
});
