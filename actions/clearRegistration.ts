import { ClearRegistrationAction } from "../types/actions/clearRegistration";
import { CLEAR_REGISTRATION } from "../constants/actions";

export const clearRegistration = (): ClearRegistrationAction => ({
  type: CLEAR_REGISTRATION,
});
