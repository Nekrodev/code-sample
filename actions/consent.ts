import {
  UpdateConsentPrivacy,
  UpdateLegalPrivacy,
} from "../types/actions/Consent";
import {
  UPDATE_CONSENT_LEGAL,
  UPDATE_CONSENT_PRIVACY,
} from "../constants/actions";

export const updateConsentPrivacy = (
  confirmed: boolean
): UpdateConsentPrivacy => ({
  type: UPDATE_CONSENT_PRIVACY,
  payload: { confirmed },
});

export const updateLegalPrivacy = (confirmed: boolean): UpdateLegalPrivacy => ({
  type: UPDATE_CONSENT_LEGAL,
  payload: { confirmed },
});
