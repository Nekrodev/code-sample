import {
  SaveProfileDataAction,
  ProfileDataSavedAction,
  GetProfileDataAction,
  GotProfileDataAction,
  SaveProfileDataFailedAction,
  GetProfileDataFailedAction,
} from "../types/actions/data";
import {
  SAVE_PROFILE_DATA,
  PROFILE_DATA_SAVED,
  GET_PROFILE_DATA,
  GOT_PROFILE_DATA,
  SAVE_PROFILE_DATA_FAILED,
  GET_PROFILE_DATA_FAILED,
} from "../constants/actions";
import { AmbassadorProfileData } from "../types/reduxInterfaces";

export const saveProfileData = (
  profileData: AmbassadorProfileData
): SaveProfileDataAction => ({
  type: SAVE_PROFILE_DATA,
  payload: profileData,
});

export const saveProfileDataFailed = (
  error: Error
): SaveProfileDataFailedAction => ({
  type: SAVE_PROFILE_DATA_FAILED,
  payload: { error },
});

export const profileDataSaved = (
  profileData: AmbassadorProfileData
): ProfileDataSavedAction => ({
  type: PROFILE_DATA_SAVED,
  payload: profileData,
});

export const getProfileData = (): GetProfileDataAction => ({
  type: GET_PROFILE_DATA,
});

export const gotProfileData = (
  profileData: AmbassadorProfileData
): GotProfileDataAction => ({
  type: GOT_PROFILE_DATA,
  payload: profileData,
});

export const getProfileDataFailed = (
  error: Error
): GetProfileDataFailedAction => ({
  type: GET_PROFILE_DATA_FAILED,
  payload: { error },
});
