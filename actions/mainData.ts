import { AmbassadorTypeChosen } from "../types/actions/MainData";
import { AmbassadorType } from "../types/AmbassadorType";
import UpdatePayload from "../../common/types/UpdatePayload";
import {
  UPDATE_COMPANY,
  UPDATE_PRIVATE_AMBASSADOR_DATA,
  AMBASSADOR_TYPE_CHOSEN,
} from "../constants/actions";

export const ambassadorTypeChosen = (
  value: AmbassadorType
): AmbassadorTypeChosen => ({
  type: AMBASSADOR_TYPE_CHOSEN,
  payload: { value },
});

export const updateCompany = (value: UpdatePayload<string>) => ({
  type: UPDATE_COMPANY,
  payload: value,
});

export const updatePrivateAmbassador = (value: UpdatePayload<string>) => ({
  type: UPDATE_PRIVATE_AMBASSADOR_DATA,
  payload: value,
});
