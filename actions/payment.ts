import { AccountType } from "../types/AccountType";
import {
  PaymentAccountTypeChosen,
  UpdatePaymentData,
  ClearPaymentData,
} from "../types/actions/Payment";
import {
  CLEAR_PAYMENT_DATA,
  PAYMENT_ACCOUNT_TYPE_CHOSEN,
  UPDATE_PAYMENT_DATA,
} from "../constants/actions";

export const paymentAccountTypeChosen = (
  accountType: AccountType
): PaymentAccountTypeChosen => ({
  type: PAYMENT_ACCOUNT_TYPE_CHOSEN,
  payload: { accountType },
});

export const updatePaymentData = (paymentData: string): UpdatePaymentData => ({
  type: UPDATE_PAYMENT_DATA,
  payload: { paymentData },
});

export const clearPaymentData = (): ClearPaymentData => ({
  type: CLEAR_PAYMENT_DATA,
});
