import {
  GotRecommendationsDataAction,
  GetRecommendationsDataAction,
  GetRecommendationsDataFailedAction,
} from "../types/actions/AmbassadorRecommendation";
import {
  AmbassadorRecommendation,
  PaginationOption,
} from "../types/AmbassadorRecommendation";
import {
  GOT_RECOMMENDATIONS_DATA,
  GET_RECOMMENDATIONS_DATA,
  GET_RECOMMENDATIONS_DATA_FAILED,
  SET_RECOMMENDATIONS_TABLE_ORDER,
  SET_RECOMMENDATIONS_TABLE_PAGINATION,
  UPDATE_RECOMMENDATIONS_FILTERS,
  SET_RESEND_EMAIL_MODAL_STATE,
  UPDATE_RECOMMENDATIONS_COLUMN_VISIBILITY,
  DELETE_RECOMMENDATION_ROW,
  DELETE_RECOMMENDATION_ROW_FAILED,
  RECOMMENDATION_ROW_DELETED,
  CLEAR_RECOMMENDATIONS_FILTERS,
} from "../constants/actions";
import {
  GetClaimsRequestParams,
  RecommendationsColumnsVisibilityState,
} from "../types/reduxInterfaces";
import { initialState } from "../reducers/recommendations/tableSettings";
import {
  SetOrderAction,
  SetPaginationAction,
  UpdateRecommendationsFiltersAction,
  UpdateColumnVisibilityAction,
  DeleteRecommendationRowAction,
  DeleteRecommendationRowFailedAction,
  RecommendationRowDeletedAction,
  ClearRecommendationsFiltersAction,
} from "../types/actions/recommendationsTableSettings";
import { SetModalStateAction } from "../types/actions/modalState";

export const gotRecommendationData = (
  recommendations: AmbassadorRecommendation[],
  totalSize: number
): GotRecommendationsDataAction => ({
  type: GOT_RECOMMENDATIONS_DATA,
  payload: { recommendations, totalSize },
});

export const getRecommendationData = (
  requestParams: GetClaimsRequestParams
): GetRecommendationsDataAction => ({
  type: GET_RECOMMENDATIONS_DATA,
  payload: { requestParams: requestParams || initialState },
});

export const getRecommendationDataFailed = (
  error: Error
): GetRecommendationsDataFailedAction => ({
  type: GET_RECOMMENDATIONS_DATA_FAILED,
  payload: { error },
});

export const setTableOrder = (
  orderDirection: string,
  orderBy: string
): SetOrderAction => ({
  type: SET_RECOMMENDATIONS_TABLE_ORDER,
  payload: { orderBy, orderDirection },
});

export const setPagination = (
  page: number,
  recordsPerPage: PaginationOption
): SetPaginationAction => ({
  type: SET_RECOMMENDATIONS_TABLE_PAGINATION,
  payload: { page, recordsPerPage },
});

export const updateRecommendationsFilters = (
  filters: { key: string; value: string }[]
): UpdateRecommendationsFiltersAction => ({
  type: UPDATE_RECOMMENDATIONS_FILTERS,
  payload: { filters },
});

export const clearRecommendationsFilters = (): ClearRecommendationsFiltersAction => ({
  type: CLEAR_RECOMMENDATIONS_FILTERS,
});

export const setModalState = (
  recommendationId: number,
  email: string
): SetModalStateAction => ({
  type: SET_RESEND_EMAIL_MODAL_STATE,
  payload: { recommendationId, email },
});

export const updateColumnsVisibility = (
  newState: RecommendationsColumnsVisibilityState
): UpdateColumnVisibilityAction => ({
  type: UPDATE_RECOMMENDATIONS_COLUMN_VISIBILITY,
  payload: { newState },
});

export const deleteRecommendationRow = (
  id: number
): DeleteRecommendationRowAction => ({
  type: DELETE_RECOMMENDATION_ROW,
  payload: { id },
});

export const deleteRecommendationRowFailed = (
  error: Error
): DeleteRecommendationRowFailedAction => ({
  type: DELETE_RECOMMENDATION_ROW_FAILED,
  payload: { error },
});

export const recommendationRowDeleted = (
  id: number
): RecommendationRowDeletedAction => ({
  type: RECOMMENDATION_ROW_DELETED,
  payload: { id },
});
