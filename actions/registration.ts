import {
  ValidateRegistrationDataAction,
  SaveRegistrationDataAction,
  ValidateRegistrationDataFailedAction,
  SaveRegistrationDataFailedAction,
  RegistrationDataValidatedAction,
  RegistrationDataSavedAction,
} from "../types/actions/registration";
import {
  VALIDATE_REGISTRATION_DATA,
  SAVE_REGISTRATION_DATA,
  REGISTRATION_DATA_VALIDATION_FAILED,
  REGISTRATION_DATA_SAVING_FAILED,
  REGISTRATION_DATA_VALIDATED,
  REGISTRATION_DATA_SAVED,
} from "../constants/actions";

export const validateRegistrationData = (): ValidateRegistrationDataAction => ({
  type: VALIDATE_REGISTRATION_DATA,
});

export const saveRegistrationData = (): SaveRegistrationDataAction => ({
  type: SAVE_REGISTRATION_DATA,
});

export const registrationDataValidated = (): RegistrationDataValidatedAction => ({
  type: REGISTRATION_DATA_VALIDATED,
});

export const registrationDataSaved = (): RegistrationDataSavedAction => ({
  type: REGISTRATION_DATA_SAVED,
});

export const registrationDataValidationFailed = (
  error: Error
): ValidateRegistrationDataFailedAction => ({
  type: REGISTRATION_DATA_VALIDATION_FAILED,
  payload: { error },
});

export const saveRegistrationDataFailed = (
  error: Error
): SaveRegistrationDataFailedAction => ({
  type: REGISTRATION_DATA_SAVING_FAILED,
  payload: { error },
});
