export enum ambassadorTypeOptions {
  none = 1,
  PrivateAmbassador = 2,
  Company = 3,
}

export const mainDataUpdateKeys = {
  email: "email",
  phone: "phone",
  street: "street",
  zipCity: "zipCity",
  countryId: "countryId",
  repeatedEmail: "repeatedEmail",
  companyName: "companyName",
  taxId: "taxId",
  contactPersonFirstName: "firstName",
  contactPersonLastName: "lastName",
  website: "website",
  firstName: "firstName",
  lastName: "lastName",
};
