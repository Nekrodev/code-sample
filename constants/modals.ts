import Address from "../components/AmbassadorAccount/AmbassadorProfile/EditElements/Address";
import Payment from "../components/AmbassadorAccount/AmbassadorProfile/EditElements/Payment";
import DeleteAccountOverlay from "../components/AmbassadorAccount/AmbassadorProfile/EditElements/DeleteAccountOverlay";
import PrivateAmbassador from "../components/AmbassadorAccount/AmbassadorProfile/EditElements/PrivateAmbassador";
import Company from "../components/AmbassadorAccount/AmbassadorProfile/EditElements/Company";
import { PasswordChangeAdapter } from "../../common/components/Modal/Password";
import ResendEmail from "../components/AmbassadorAccount/Recommendations/ResendEmail";
import EditRecommendationsColumns from "../components/AmbassadorAccount/Recommendations/EditColumns";
import EditClaimsColumns from "../components/AmbassadorAccount/Claims/EditColumns";

export const modalKeys = {
  address: "address",
  payment: "payment",
  password: "password",
  delete: "delete",
  privateAmbassador: "privateAmbassador",
  companyAmbassador: "companyAmbassador",
  resendEmail: "resendEmail",
  editRecommendationsColumns: "editRecommendationsColumns",
  editClaimsColumns: "editClaimsColumns",
};

export const modalComponents = {
  address: Address,
  payment: Payment,
  password: PasswordChangeAdapter,
  delete: DeleteAccountOverlay,
  privateAmbassador: PrivateAmbassador,
  companyAmbassador: Company,
  resendEmail: ResendEmail,
  editRecommendationsColumns: EditRecommendationsColumns,
  editClaimsColumns: EditClaimsColumns,
};
