export const defaultPaginationOption = 20;

export const tableFieldsMapping = {
  amount: "potential_amount",
  flightDate: "flight_date",
  passengerName: "passenger_name",
  flightRoute: "flight_route",
  reference: "internal_reference",
  isSigned: "is_used",
  route: "flight_route",
  email: "passenger_email",
  phase: "claim_phase",
  ambassadorCode: "ambassador_code",
};

export const recommendationsColumnKeys = {
  actions: "actions",
  amount: "amount",
  flightDate: "flightDate",
  passengerName: "passengerName",
  flightRoute: "flightRoute",
  reference: "reference",
  status: "status",
};

export const claimsColumnKeys = {
  ambassadorCode: "ambassadorCode",
  amount: "amount",
  flightDate: "flightDate",
  passengerName: "passengerName",
  flightRoute: "flightRoute",
  reference: "reference",
  status: "status",
};
