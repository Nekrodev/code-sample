export default {
  claims: "/api/my/claims",
  registerAmbassador: "/api/ambassador/register",
  isAmbassadorActive: "/api/ambassador/is_accepted",
  recommendations: "/api/my/recommendations",
  validateAmbassadorRegistrationData: "/api/ambassador/register-validate",
  deleteRecommendation: "/api/ambassador/delete-recommendation",
  getCodes: "/api/ambassador/codes",
  createCode: "/api/ambassador/create-code",
  deleteCode: "/api/ambassador/delete-code",
};
