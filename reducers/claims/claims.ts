import { AmbassadorClaim } from "../../types/AmbassadorClaim";
import { ClaimsAction } from "../../types/actions/AmbassadorClaims";
import {
  GOT_CLAIMS_DATA,
  GET_CLAIMS_DATA_FAILED,
} from "../../constants/actions";

type State = AmbassadorClaim[];

export const initialState: State = [];

export default function (state = initialState, action: ClaimsAction) {
  switch (action.type) {
    case GOT_CLAIMS_DATA:
      return action.payload.claims;
    case GET_CLAIMS_DATA_FAILED:
    case "CLEAR_STORAGE":
      return initialState;
    default:
      return state;
  }
}
