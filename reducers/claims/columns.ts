import { ClearStorageAction } from "../../../common/types/actions/clearStorage";
import { UpdateClaimsColumnsVisibilityAction } from "../../types/actions/claimsTableSettings";
import { UPDATE_CLAIMS_COLUMNS_VISIBILITY } from "../../constants/actions";
import { ClaimsColumnsVisibilityState } from "../../types/reduxInterfaces";

export const initialState: ClaimsColumnsVisibilityState = {
  ambassadorCode: true,
  amount: true,
  flightDate: true,
  passengerName: true,
  flightRoute: true,
  reference: true,
  status: true,
};

const columns = (
  state = initialState,
  action: ClearStorageAction | UpdateClaimsColumnsVisibilityAction
) => {
  if (action.type === UPDATE_CLAIMS_COLUMNS_VISIBILITY) {
    return { ...state, ...action.payload.newState };
  }

  if (action.type === "CLEAR_STORAGE") {
    return initialState;
  }

  return state;
};

export default columns;
