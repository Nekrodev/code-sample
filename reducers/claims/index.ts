import loadingState, * as fromLoadingState from "./loadingStatus";
import claims, * as fromClaims from "./claims";
import tableSettings, * as fromTableSettings from "./tableSettings";
import columns, * as fromColumns from "./columns";
import { combineReducers } from "redux";

const claimsState = combineReducers({
  loadingState,
  claims,
  tableSettings,
  columns,
});

export default claimsState;

export const initialState = {
  loadingState: fromLoadingState.initialState,
  claims: fromClaims.initialState,
  tableSettings: fromTableSettings.initialState,
  columns: fromColumns.initialState,
};

export type ClaimsState = ReturnType<typeof claimsState>;

export const getLoadingState = (state: ClaimsState) =>
  fromLoadingState.getLoadingStatus(state.loadingState);
export const getClaims = (state: ClaimsState) => state.claims;
export const getTableSettings = (state: ClaimsState) =>
  fromTableSettings.getTableSettings(state.tableSettings);
export const getColumnsVisibility = (state: ClaimsState) => state.columns;
