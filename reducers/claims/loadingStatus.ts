import { ClaimsAction } from "../../types/actions/AmbassadorClaims";
import {
  GET_CLAIMS_DATA,
  GET_CLAIMS_DATA_FAILED,
  GOT_CLAIMS_DATA,
} from "../../constants/actions";
import { combineReducers } from "redux";

type State = {
  isLoading: boolean;
  error: string;
};

export const initialState: State = {
  isLoading: false,
  error: "",
};

const isLoading = (state = initialState.isLoading, action: ClaimsAction) => {
  switch (action.type) {
    case GET_CLAIMS_DATA:
      return true;
    case GET_CLAIMS_DATA_FAILED:
    case GOT_CLAIMS_DATA:
    case "CLEAR_STORAGE":
      return initialState.isLoading;
    default:
      return state;
  }
};

const error = (state = initialState.error, action: ClaimsAction) => {
  switch (action.type) {
    case GET_CLAIMS_DATA_FAILED:
      return action.payload.error.message;
    case GET_CLAIMS_DATA:
    case GOT_CLAIMS_DATA:
    case "CLEAR_STORAGE":
    default:
      return initialState.error;
  }
};

export const getLoadingStatus = (state: State) => ({
  isLoading: state.isLoading,
  error: state.error,
});

export default combineReducers({
  isLoading,
  error,
});
