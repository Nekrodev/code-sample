import { TableClaimsAction } from "../../types/actions/claimsTableSettings";
import {
  CLEAR_CLAIMS_FILTERS,
  UPDATE_CLAIMS_FILTERS,
  SET_CLAIMS_TABLE_ORDER,
  GOT_CLAIMS_DATA,
  GET_CLAIMS_DATA,
  GET_CLAIMS_DATA_FAILED,
  SET_CLAIMS_TABLE_PAGINATION,
} from "../../constants/actions";
import { combineReducers } from "redux";
import { TableSettings } from "../../types/reduxInterfaces";
import { ClaimsAction } from "../../types/actions/AmbassadorClaims";
import { defaultPaginationOption } from "../../constants/table";

export const initialState: TableSettings = {
  recordsPerPage: defaultPaginationOption,
  orderBy: "flightDate",
  orderDirection: "desc",
  page: 1,
  totalSize: 1,
  filters: [],
};

const recordsPerPage = (
  state = initialState.recordsPerPage,
  action: TableClaimsAction
) => {
  switch (action.type) {
    case SET_CLAIMS_TABLE_PAGINATION:
      return action.payload.recordsPerPage;
    case "CLEAR_STORAGE":
      return initialState.recordsPerPage;
    default:
      return state;
  }
};

const totalSize = (
  state = initialState.totalSize,
  action: TableClaimsAction | ClaimsAction
) => {
  switch (action.type) {
    case GOT_CLAIMS_DATA:
      return action.payload.totalSize;
    case "CLEAR_STORAGE":
    case GET_CLAIMS_DATA_FAILED:
      return initialState.totalSize;
    case GET_CLAIMS_DATA:
    default:
      return state;
  }
};

const orderBy = (state = initialState.orderBy, action: TableClaimsAction) => {
  switch (action.type) {
    case SET_CLAIMS_TABLE_ORDER:
      return action.payload.orderBy;
    case "CLEAR_STORAGE":
      return initialState.orderBy;
    default:
      return state;
  }
};

const orderDirection = (
  state = initialState.orderDirection,
  action: TableClaimsAction
) => {
  switch (action.type) {
    case SET_CLAIMS_TABLE_ORDER:
      return action.payload.orderDirection;
    case "CLEAR_STORAGE":
      return initialState.orderDirection;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action: TableClaimsAction) => {
  switch (action.type) {
    case "CLEAR_STORAGE":
    case CLEAR_CLAIMS_FILTERS:
      return initialState.filters;
    case UPDATE_CLAIMS_FILTERS:
      return action.payload.filters;
    default:
      return state;
  }
};

const page = (state = initialState.page, action: TableClaimsAction) => {
  switch (action.type) {
    case "CLEAR_STORAGE":
    case UPDATE_CLAIMS_FILTERS:
    case SET_CLAIMS_TABLE_ORDER:
      return initialState.page;
    case SET_CLAIMS_TABLE_PAGINATION:
      return action.payload.page;
    default:
      return state;
  }
};

export const getTableSettings = (state: TableSettings) => ({
  recordsPerPage: state.recordsPerPage,
  orderBy: state.orderBy,
  orderDirection: state.orderDirection,
  page: state.page,
  filters: state.filters,
  totalSize: state.totalSize,
});

export default combineReducers({
  recordsPerPage,
  orderBy,
  orderDirection,
  filters,
  page,
  totalSize,
});
