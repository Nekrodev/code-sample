import { AmbassadorCodesActions } from "../../types/actions/AmbassadorCodes";
import {
  UPDATE_AMBASSADOR_CODES,
  ADD_AMBASSADOR_CODE,
  REMOVE_AMBASSADOR_CODE,
} from "../../constants/actions";
import { AmbassadorCode } from "../../types/AmbassadorCode";

export const initialState = null;

const codes = (
  state: AmbassadorCode[] | null = initialState,
  action: AmbassadorCodesActions
): AmbassadorCode[] | null => {
  switch (action.type) {
    case UPDATE_AMBASSADOR_CODES: {
      return action.payload.codes;
    }
    case ADD_AMBASSADOR_CODE: {
      if (state === null) {
        return [action.payload.code];
      }

      return [...state, action.payload.code];
    }
    case REMOVE_AMBASSADOR_CODE: {
      return (
        state &&
        state.filter((value) => {
          return value.id !== action.payload.id;
        })
      );
    }
    case "CLEAR_STORAGE": {
      return initialState;
    }
    default: {
      return state;
    }
  }
};

export type CodesState = ReturnType<typeof codes>;

export const getAmbassadorCodes = (state: CodesState) => state;

export default codes;
