import profile, * as fromProfile from "./profile";
import registration, * as fromRegistration from "./registration";
import recommendation, * as fromRecommendation from "./recommendations";
import codes, * as fromCodes from "./codes";
import claims, * as fromClaims from "./claims";
import lastActionType, * as fromLastActionType from "../../common/reducers/lastAction";
import { combineReducers } from "redux";

const root = combineReducers({
  profile,
  registration,
  recommendation,
  codes,
  claims,
  lastActionType,
});

export default root;

export const initialState = {
  profile: fromProfile.initialState,
  registration: fromRegistration.initialState,
  recommendation: fromRecommendation.initialState,
  codes: fromCodes.initialState,
  claims: fromClaims.initialState,
  lastActionType: fromLastActionType.initialState,
};

export type RootState = ReturnType<typeof root>;
