import mainData, * as fromMainData from "./mainData";
import payment, * as fromPayment from "./payment";
import requestState, * as fromRequestState from "./requestState";
import { combineReducers } from "redux";
import { ambassadorTypeOptions } from "../../constants/mainData";
import { BaseData } from "../../types/reduxInterfaces";

const profile = combineReducers({
  mainData,
  payment,
  requestState,
});

export default profile;

export const initialState = {
  mainData: fromMainData.initialState,
  payment: fromPayment.initialState,
  requestState: fromRequestState.initialState,
};

export type ProfileState = ReturnType<typeof profile>;

export const getCompanyData = (state: ProfileState) =>
  fromMainData.getCompanyData(state.mainData);
export const getPrivateAmbassadorData = (state: ProfileState) =>
  fromMainData.getPrivateAmbassadorData(state.mainData);
export const getAmbassadorType = (state: ProfileState) =>
  fromMainData.getAmbassadorType(state.mainData);

export const getPaymentData = (state: ProfileState) => state.payment;
export const getPaymentAccountType = (state: ProfileState) =>
  fromPayment.getAccountType(state.payment);

export const getAmbassadorBaseData = (
  state: ProfileState
): Partial<BaseData> | null => {
  const type = getAmbassadorType(state);
  if (type === ambassadorTypeOptions.Company) {
    return getCompanyData(state);
  }

  if (type === ambassadorTypeOptions.PrivateAmbassador) {
    return getPrivateAmbassadorData(state);
  }

  return null;
};

export const getSavingStatus = (state: ProfileState) => ({
  isSaving: fromRequestState.getSavingStatus(state.requestState),
  error: fromRequestState.getSavingError(state.requestState),
});

export const getLoadingStatus = (state: ProfileState) => ({
  error: fromRequestState.getLoadingError(state.requestState),
  isLoading: fromRequestState.getLoadingStatus(state.requestState),
});