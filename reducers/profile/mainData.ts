import { combineReducers } from "redux";
import {
  GET_PROFILE_DATA_FAILED,
  PROFILE_DATA_SAVED,
  GOT_PROFILE_DATA,
} from "../../constants/actions";
import {
  CompanyAction,
  PrivateAmbassadorAction,
} from "../../types/actions/MainData";
import { DataAction } from "../../types/actions/data";
import { ambassadorTypeOptions } from "../../constants/mainData";
import { Company, BaseData } from "../../types/reduxInterfaces";
import { AmbassadorType } from "../../types/AmbassadorType";

type State = {
  ambassadorType: AmbassadorType;
  privateAmbassador: Partial<BaseData> | null;
  company: Partial<Company> | null;
};

export const initialState: State = {
  ambassadorType: ambassadorTypeOptions.none,
  privateAmbassador: null,
  company: null,
};

const ambassadorType = (
  state = initialState.ambassadorType,
  action: CompanyAction | PrivateAmbassadorAction | DataAction
) => {
  switch (action.type) {
    case GOT_PROFILE_DATA:
      return action.payload.ambassadorType;
    case GET_PROFILE_DATA_FAILED:
    case "CLEAR_STORAGE": {
      return initialState.ambassadorType;
    }
    default:
      return state;
  }
};

const privateAmbassador = (
  state = initialState.privateAmbassador,
  action: PrivateAmbassadorAction | DataAction
) => {
  switch (action.type) {
    case PROFILE_DATA_SAVED: {
      const changes = action.payload.privateAmbassador;

      return changes ? { ...state, ...changes } : state;
    }
    case GOT_PROFILE_DATA:
      return action.payload.privateAmbassador;
    case GET_PROFILE_DATA_FAILED:
    case "CLEAR_STORAGE": {
      return initialState.privateAmbassador;
    }
    default:
      return state;
  }
};

const company = (
  state = initialState.company,
  action: CompanyAction | DataAction
) => {
  switch (action.type) {
    case PROFILE_DATA_SAVED: {
      const changes = action.payload.company;

      return changes ? { ...state, ...changes } : state;
    }
    case GOT_PROFILE_DATA:
      return action.payload.company;
    case GET_PROFILE_DATA_FAILED:
    case "CLEAR_STORAGE": {
      return initialState.company;
    }
    default:
      return state;
  }
};

export const getPrivateAmbassadorData = (state: State) =>
  state.privateAmbassador;
export const getCompanyData = (state: State) => state.company;
export const getAmbassadorType = (state: State) => state.ambassadorType;

export default combineReducers({
  company,
  ambassadorType,
  privateAmbassador,
});
