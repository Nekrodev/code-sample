import { Payment } from "../../types/reduxInterfaces";
import { PaymentAction } from "../../types/actions/Payment";
import { DataAction } from "../../types/actions/data";
import { accountTypes } from "../../types/AccountType";
import {
  PROFILE_DATA_SAVED,
  GOT_PROFILE_DATA,
  PAYMENT_ACCOUNT_TYPE_CHOSEN,
  GET_PROFILE_DATA_FAILED,
  CLEAR_PAYMENT_DATA,
} from "../../constants/actions";

export const initialState: Payment = {
  bankAccount: "",
  paypalEmail: "",
  accountType: accountTypes.none,
};
const normalizePaymentData = (payment: Partial<Payment> | null) => {
  const paymentData = payment || initialState;

  return {
    bankAccount: paymentData.bankAccount || initialState.bankAccount,
    paypalEmail: paymentData.paypalEmail || initialState.paypalEmail,
    accountType: paymentData.accountType || initialState.accountType,
  };
};

const payment = (
  state = initialState,
  action: PaymentAction | DataAction
): Payment => {
  switch (action.type) {
    case PROFILE_DATA_SAVED:
      if (action.payload.payment === null) {
        return state;
      } else {
        return normalizePaymentData(action.payload.payment);
      }
    case GOT_PROFILE_DATA:
      return normalizePaymentData(action.payload.payment);
    case PAYMENT_ACCOUNT_TYPE_CHOSEN:
      return { ...state, accountType: action.payload.accountType };
    case GET_PROFILE_DATA_FAILED:
    case CLEAR_PAYMENT_DATA:
    case "CLEAR_STORAGE":
      return initialState;
    default:
      return state;
  }
};

export const getBankAccount = (state: Payment) => state.bankAccount;
export const getPaypallEmail = (state: Payment) => state.paypalEmail;
export const getAccountType = (state: Payment) => state.accountType;

export default payment;
