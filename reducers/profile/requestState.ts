import { combineReducers } from "redux";
import { DataAction } from "../../types/actions/data";
import {
  SAVE_PROFILE_DATA,
  SAVE_PROFILE_DATA_FAILED,
  PROFILE_DATA_SAVED,
  GOT_PROFILE_DATA,
  GET_PROFILE_DATA,
  GET_PROFILE_DATA_FAILED,
} from "../../constants/actions";

type State = {
  saving: boolean;
  savingError: Error | null;
  loadingError: Error | null;
  loading: boolean;
};

export const initialState: State = {
  saving: false,
  savingError: null,
  loadingError: null,
  loading: false,
};

const loading = (state = initialState.loading, action: DataAction): boolean => {
  switch (action.type) {
    case GET_PROFILE_DATA: {
      return true;
    }
    case GOT_PROFILE_DATA:
    case GET_PROFILE_DATA_FAILED:
    case "CLEAR_STORAGE": {
      return initialState.loading;
    }
    default:
      return state;
  }
};

const saving = (
  state = initialState.saving,
  action: DataAction
): typeof initialState.saving => {
  switch (action.type) {
    case SAVE_PROFILE_DATA: {
      return true;
    }
    case "CLEAR_STORAGE": {
      return initialState.saving;
    }
    case PROFILE_DATA_SAVED:
    case SAVE_PROFILE_DATA_FAILED:
      return false;
    default:
      return state;
  }
};
const loadingError = (
  state = initialState.loadingError,
  action: DataAction
): Error | null => {
  switch (action.type) {
    case GET_PROFILE_DATA_FAILED:
      return action.payload.error;
    case GOT_PROFILE_DATA:
    case "CLEAR_STORAGE":
    case PROFILE_DATA_SAVED: {
      return initialState.loadingError;
    }
    default:
      return initialState.loadingError;
  }
};

const savingError = (
  state = initialState.savingError,
  action: DataAction
): Error | null => {
  switch (action.type) {
    case SAVE_PROFILE_DATA_FAILED:
      return action.payload.error;
    case GOT_PROFILE_DATA:
    case "CLEAR_STORAGE":
    case PROFILE_DATA_SAVED: {
      return initialState.savingError;
    }
    default:
      return initialState.savingError;
  }
};

export const getSavingStatus = (state: State) => state.saving;
export const getLoadingError = (state: State) => state.loadingError;
export const getSavingError = (state: State) => state.savingError;
export const getLoadingStatus = (state: State) => state.loading;

export default combineReducers({
  saving,
  loadingError,
  savingError,
  loading,
});
