import { UpdateColumnVisibilityAction } from "../../types/actions/recommendationsTableSettings";
import { UPDATE_RECOMMENDATIONS_COLUMN_VISIBILITY } from "../../constants/actions";
import { ClearStorageAction } from "../../../common/types/actions/clearStorage";
import { RecommendationsColumnsVisibilityState } from "../../types/reduxInterfaces";

export const initialState: RecommendationsColumnsVisibilityState = {
  actions: true,
  amount: true,
  flightDate: true,
  passengerName: true,
  flightRoute: true,
  reference: true,
  status: true,
};

const columns = (
  state = initialState,
  action: UpdateColumnVisibilityAction | ClearStorageAction
) => {
  if (action.type === UPDATE_RECOMMENDATIONS_COLUMN_VISIBILITY) {
    return { ...state, ...action.payload.newState };
  }

  if (action.type === "CLEAR_STORAGE") {
    return initialState;
  }

  return state;
};

export default columns;
