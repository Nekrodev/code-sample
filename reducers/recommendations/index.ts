import loadingState, * as fromLoadingState from "./loadingStatus";
import modalState, * as fromModalState from "./resendEmailModalState";
import recommendations, * as fromRecommendation from "./recommendations";
import tableSettings, * as fromTableSettings from "./tableSettings";
import columns, * as fromColumns from "./columns";
import { combineReducers } from "redux";

const recommendation = combineReducers({
  loadingState,
  recommendations,
  tableSettings,
  modalState,
  columns,
});

export default recommendation;

export const initialState = {
  loadingState: fromLoadingState.initialState,
  recommendations: fromRecommendation.initialState,
  tableSettings: fromTableSettings.initialState,
  modalState: fromModalState.initialState,
  columns: fromColumns.initialState,
};

export type RecommendationState = ReturnType<typeof recommendation>;

export const getLoadingState = (state: RecommendationState) =>
  fromLoadingState.getLoadingStatus(state.loadingState);
export const getRecommendations = (state: RecommendationState) =>
  state.recommendations;
export const getTableSettings = (state: RecommendationState) =>
  fromTableSettings.getTableSettings(state.tableSettings);
export const getResendEmailModalState = (state: RecommendationState) =>
  fromModalState.getResendEmailModalState(state.modalState);
export const getColumnsVisibility = (state: RecommendationState) =>
  state.columns;
