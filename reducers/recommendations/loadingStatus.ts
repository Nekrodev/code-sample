import { RecommendationsAction } from "../../types/actions/AmbassadorRecommendation";
import {
  GET_RECOMMENDATIONS_DATA,
  GET_RECOMMENDATIONS_DATA_FAILED,
  GOT_RECOMMENDATIONS_DATA,
  DELETE_RECOMMENDATION_ROW,
  DELETE_RECOMMENDATION_ROW_FAILED,
  RECOMMENDATION_ROW_DELETED,
} from "../../constants/actions";
import { combineReducers } from "redux";
import { TableAction } from "../../types/actions/recommendationsTableSettings";

type State = {
  isLoading: boolean;
  error: string;
};

export const initialState: State = {
  isLoading: false,
  error: "",
};

const isLoading = (
  state = initialState.isLoading,
  action: RecommendationsAction | TableAction
) => {
  switch (action.type) {
    case GET_RECOMMENDATIONS_DATA:
    case DELETE_RECOMMENDATION_ROW:
      return true;
    case RECOMMENDATION_ROW_DELETED:
    case DELETE_RECOMMENDATION_ROW_FAILED:
    case GET_RECOMMENDATIONS_DATA_FAILED:
    case GOT_RECOMMENDATIONS_DATA:
    case "CLEAR_STORAGE":
      return initialState.isLoading;
    default:
      return state;
  }
};

const error = (
  state = initialState.error,
  action: RecommendationsAction | TableAction
) => {
  switch (action.type) {
    case GET_RECOMMENDATIONS_DATA_FAILED:
    case DELETE_RECOMMENDATION_ROW_FAILED:
      return action.payload.error.message;
    case GET_RECOMMENDATIONS_DATA:
    case GOT_RECOMMENDATIONS_DATA:
    case "CLEAR_STORAGE":
    default:
      return initialState.error;
  }
};

export const getLoadingStatus = (state: State) => ({
  isLoading: state.isLoading,
  error: state.error,
});

export default combineReducers({
  isLoading,
  error,
});
