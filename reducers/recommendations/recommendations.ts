import { AmbassadorRecommendation } from "../../types/AmbassadorRecommendation";
import { RecommendationsAction } from "../../types/actions/AmbassadorRecommendation";
import {
  GOT_RECOMMENDATIONS_DATA,
  GET_RECOMMENDATIONS_DATA_FAILED,
  RECOMMENDATION_ROW_DELETED,
} from "../../constants/actions";
import { RecommendationRowDeletedAction } from "../../types/actions/recommendationsTableSettings";

type State = AmbassadorRecommendation[];

export const initialState: State = [];

export default function (
  state = initialState,
  action: RecommendationsAction | RecommendationRowDeletedAction
) {
  switch (action.type) {
    case RECOMMENDATION_ROW_DELETED:
      return state.filter((element) => element.id !== action.payload.id);
    case GOT_RECOMMENDATIONS_DATA:
      return action.payload.recommendations;
    case GET_RECOMMENDATIONS_DATA_FAILED:
    case "CLEAR_STORAGE":
      return initialState;
    default:
      return state;
  }
}
