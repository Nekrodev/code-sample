import { combineReducers } from "redux";
import { SetModalStateAction } from "../../types/actions/modalState";
import { SET_RESEND_EMAIL_MODAL_STATE } from "../../constants/actions";

type State = {
  recommendationId: number;
  email: string;
};

export const initialState: State = {
  recommendationId: -1,
  email: "",
};

const recommendationId = (
  state = initialState.recommendationId,
  action: SetModalStateAction
) => {
  return action.type === SET_RESEND_EMAIL_MODAL_STATE
    ? action.payload.recommendationId
    : state;
};

const email = (state = initialState.email, action: SetModalStateAction) => {
  return action.type === SET_RESEND_EMAIL_MODAL_STATE
    ? action.payload.email
    : state;
};

export const getResendEmailModalState = (state: State) => ({
  recommendationId: state.recommendationId,
  email: state.email,
});

export default combineReducers({
  recommendationId,
  email,
});
