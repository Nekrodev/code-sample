import { defaultPaginationOption } from "../../constants/table";
import { TableAction } from "../../types/actions/recommendationsTableSettings";
import {
  SET_RECOMMENDATIONS_TABLE_PAGINATION,
  CLEAR_RECOMMENDATIONS_FILTERS,
  UPDATE_RECOMMENDATIONS_FILTERS,
  SET_RECOMMENDATIONS_TABLE_ORDER,
  GOT_RECOMMENDATIONS_DATA,
  GET_RECOMMENDATIONS_DATA,
  GET_RECOMMENDATIONS_DATA_FAILED,
  RECOMMENDATION_ROW_DELETED,
} from "../../constants/actions";
import { combineReducers } from "redux";
import { TableSettings } from "../../types/reduxInterfaces";
import { RecommendationsAction } from "../../types/actions/AmbassadorRecommendation";

export const initialState: TableSettings = {
  recordsPerPage: defaultPaginationOption,
  orderBy: "flightDate",
  orderDirection: "desc",
  page: 1,
  totalSize: 1,
  filters: [],
};

const recordsPerPage = (
  state = initialState.recordsPerPage,
  action: TableAction
) => {
  switch (action.type) {
    case SET_RECOMMENDATIONS_TABLE_PAGINATION:
      return action.payload.recordsPerPage;
    case "CLEAR_STORAGE":
      return initialState.recordsPerPage;
    default:
      return state;
  }
};

const totalSize = (
  state = initialState.totalSize,
  action: TableAction | RecommendationsAction
) => {
  switch (action.type) {
    case RECOMMENDATION_ROW_DELETED:
      return state - 1;
    case GOT_RECOMMENDATIONS_DATA:
      return action.payload.totalSize;
    case "CLEAR_STORAGE":
    case GET_RECOMMENDATIONS_DATA_FAILED:
      return initialState.totalSize;
    case GET_RECOMMENDATIONS_DATA:
    default:
      return state;
  }
};

const orderBy = (state = initialState.orderBy, action: TableAction) => {
  switch (action.type) {
    case SET_RECOMMENDATIONS_TABLE_ORDER:
      return action.payload.orderBy;
    case "CLEAR_STORAGE":
      return initialState.orderBy;
    default:
      return state;
  }
};

const orderDirection = (
  state = initialState.orderDirection,
  action: TableAction
) => {
  switch (action.type) {
    case SET_RECOMMENDATIONS_TABLE_ORDER:
      return action.payload.orderDirection;
    case "CLEAR_STORAGE":
      return initialState.orderDirection;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action: TableAction) => {
  switch (action.type) {
    case "CLEAR_STORAGE":
    case CLEAR_RECOMMENDATIONS_FILTERS:
      return initialState.filters;
    case UPDATE_RECOMMENDATIONS_FILTERS:
      return action.payload.filters;
    default:
      return state;
  }
};

const page = (state = initialState.page, action: TableAction) => {
  switch (action.type) {
    case "CLEAR_STORAGE":
    case UPDATE_RECOMMENDATIONS_FILTERS:
    case SET_RECOMMENDATIONS_TABLE_ORDER:
      return initialState.page;
    case SET_RECOMMENDATIONS_TABLE_PAGINATION:
      return action.payload.page;
    default:
      return state;
  }
};

export const getTableSettings = (state: TableSettings) => ({
  recordsPerPage: state.recordsPerPage,
  orderBy: state.orderBy,
  orderDirection: state.orderDirection,
  page: state.page,
  filters: state.filters,
  totalSize: state.totalSize,
});

export default combineReducers({
  recordsPerPage,
  orderBy,
  orderDirection,
  filters,
  page,
  totalSize,
});
