import { AmbassadorCodeAction } from "../../types/actions/AmbassadorCode";
import {
  UPDATE_AMBASSADOR_CODE,
  CLEAR_REGISTRATION,
} from "../../constants/actions";

export const initialState = "";

const ambassadorCode = (state = initialState, action: AmbassadorCodeAction) => {
  switch (action.type) {
    case UPDATE_AMBASSADOR_CODE:
      return action.payload.code;
    case CLEAR_REGISTRATION:
    case "CLEAR_STORAGE":
      return initialState;
    default:
      return state;
  }
};

export default ambassadorCode;
