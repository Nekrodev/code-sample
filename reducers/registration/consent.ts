import { ConsentAction } from "../../types/actions/Consent";
import { combineReducers } from "redux";
import {
  UPDATE_CONSENT_LEGAL,
  UPDATE_CONSENT_PRIVACY,
  CLEAR_REGISTRATION,
} from "../../constants/actions";

type State = {
  consentPrivacy: boolean;
  consentLegal: boolean;
};

export const initialState: State = {
  consentPrivacy: false,
  consentLegal: false,
};

const consentLegal = (
  state = initialState.consentLegal,
  action: ConsentAction
) => {
  switch (action.type) {
    case UPDATE_CONSENT_LEGAL:
      return action.payload.confirmed;
    case CLEAR_REGISTRATION:
    case "CLEAR_STORAGE":
      return initialState.consentLegal;
    default:
      return state;
  }
};

const consentPrivacy = (
  state = initialState.consentPrivacy,
  action: ConsentAction
) => {
  switch (action.type) {
    case UPDATE_CONSENT_PRIVACY:
      return action.payload.confirmed;
    case CLEAR_REGISTRATION:
    case "CLEAR_STORAGE":
      return initialState.consentPrivacy;
    default:
      return state;
  }
};

export const getConsentLegal = (state: State) => state.consentLegal;
export const getConsentPrivacy = (state: State) => state.consentPrivacy;

export default combineReducers({ consentLegal, consentPrivacy });
