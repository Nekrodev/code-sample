import { combineReducers } from "redux";
import { RegistrationDataAction } from "../../types/actions/registration";
import { ClearStorageAction } from "../../../login/types/actions/login";
import {
  VALIDATE_REGISTRATION_DATA,
  SAVE_REGISTRATION_DATA,
  REGISTRATION_DATA_VALIDATION_FAILED,
  REGISTRATION_DATA_SAVING_FAILED,
  REGISTRATION_DATA_SAVED,
  REGISTRATION_DATA_VALIDATED,
} from "../../constants/actions";

type State = {
  isLoading: boolean;
  error: Error | null;
};

export const initialState: State = {
  isLoading: false,
  error: null,
};

const isLoading = (
  state = initialState.isLoading,
  action: RegistrationDataAction | ClearStorageAction
): typeof initialState.isLoading => {
  switch (action.type) {
    case VALIDATE_REGISTRATION_DATA:
    case SAVE_REGISTRATION_DATA: {
      return true;
    }
    case "CLEAR_STORAGE": {
      return initialState.isLoading;
    }
    case REGISTRATION_DATA_VALIDATION_FAILED:
    case REGISTRATION_DATA_SAVING_FAILED:
    case REGISTRATION_DATA_SAVED:
    case REGISTRATION_DATA_VALIDATED:
      return false;
    default:
      return state;
  }
};

const error = (
  state = initialState.error,
  action: RegistrationDataAction | ClearStorageAction
): typeof initialState.error => {
  switch (action.type) {
    case REGISTRATION_DATA_VALIDATION_FAILED:
    case REGISTRATION_DATA_SAVING_FAILED:
      return action.payload.error;
    default:
      return initialState.error;
  }
};

export const getLoadingStatus = (state: State) => state.isLoading;
export const getError = (state: State) => state.error;

export default combineReducers({
  isLoading,
  error,
});
