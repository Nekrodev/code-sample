import consent, * as fromConsent from "./consent";
import mainData, * as fromMainData from "./mainData";
import payment, * as fromPayment from "./payment";
import ambassadorCode, * as fromAmbassadorCode from "./ambassadorCode";
import lastUpdate, * as fromLastUpdate from "./lastUpdate";
import data, * as fromData from "./data";
import { combineReducers } from "redux";
import { ambassadorTypeOptions } from "../../constants/mainData";
import { BaseData, AmbassadorProfileData } from "../../types/reduxInterfaces";

const registration = combineReducers({
  consent,
  mainData,
  payment,
  ambassadorCode,
  lastUpdate,
  data,
});

export default registration;

export const initialState = {
  consent: fromConsent.initialState,
  mainData: fromMainData.initialState,
  payment: fromPayment.initialState,
  ambassadorCode: fromAmbassadorCode.initialState,
  lastUpdate: fromLastUpdate.initialState,
  data: fromData.initialState,
};

export type RegistrationState = ReturnType<typeof registration>;

export const getLoadingStatus = (state: RegistrationState) => ({
  isLoading: fromData.getLoadingStatus(state.data),
  error: fromData.getError(state.data),
});

export const getCompanyData = (state: RegistrationState) =>
  fromMainData.getCompanyData(state.mainData);
export const isCountryIdFilled = (state: RegistrationState) => {
  const companyData = fromMainData.getCompanyData(state.mainData);
  const privateData = fromMainData.getPrivateAmbassadorData(state.mainData);

  return (
    (companyData && companyData.countryId) ||
    (privateData && privateData.countryId)
  );
};
export const getPrivateAmbassadorData = (state: RegistrationState) =>
  fromMainData.getPrivateAmbassadorData(state.mainData);
export const getAmbassadorType = (state: RegistrationState) =>
  fromMainData.getAmbassadorType(state.mainData);

export const getPaymentData = (state: RegistrationState) => state.payment;
export const getPaymentAccountType = (state: RegistrationState) =>
  fromPayment.getAccountType(state.payment);

export const getAmbassadorCode = (state: RegistrationState) =>
  state.ambassadorCode;

export const getConsentLegal = (state: RegistrationState) =>
  fromConsent.getConsentLegal(state.consent);
export const getConsentPrivacy = (state: RegistrationState) =>
  fromConsent.getConsentPrivacy(state.consent);

export const getAmbassadorBaseData = (
  state: RegistrationState
): Partial<BaseData> | null => {
  const type = getAmbassadorType(state);
  const companyData = getCompanyData(state);
  if (type === ambassadorTypeOptions.Company && companyData) {
    return companyData;
  }

  const privateAmbassadorData = getPrivateAmbassadorData(state);
  if (
    type === ambassadorTypeOptions.PrivateAmbassador &&
    privateAmbassadorData
  ) {
    return privateAmbassadorData;
  }

  return null;
};

export const getAmbassadorProfileData = (
  state: RegistrationState
): AmbassadorProfileData => ({
  ambassadorType: getAmbassadorType(state),
  company: getCompanyData(state),
  privateAmbassador: getPrivateAmbassadorData(state),
  payment: getPaymentData(state),
});
