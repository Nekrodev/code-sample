import { getTimeNow } from "../../../common/services/Time";
import { ConsentAction } from "../../types/actions/Consent";
import {
  CompanyAction,
  PrivateAmbassadorAction,
} from "../../types/actions/MainData";
import { PaymentAction } from "../../types/actions/Payment";
import { AmbassadorCodeAction } from "../../types/actions/AmbassadorCode";
import {
  UPDATE_COMPANY,
  UPDATE_PRIVATE_AMBASSADOR_DATA,
  UPDATE_PAYMENT_DATA,
  UPDATE_CONSENT_PRIVACY,
  UPDATE_CONSENT_LEGAL,
  UPDATE_AMBASSADOR_CODE,
  PAYMENT_ACCOUNT_TYPE_CHOSEN,
  CLEAR_REGISTRATION,
} from "../../constants/actions";

export const initialState = getTimeNow();

export default function (
  state = initialState,
  action:
    | ConsentAction
    | CompanyAction
    | PrivateAmbassadorAction
    | PaymentAction
    | AmbassadorCodeAction
) {
  switch (action.type) {
    case "CLEAR_STORAGE":
    case CLEAR_REGISTRATION:
    case PAYMENT_ACCOUNT_TYPE_CHOSEN:
    case UPDATE_AMBASSADOR_CODE:
    case UPDATE_COMPANY:
    case UPDATE_CONSENT_LEGAL:
    case UPDATE_CONSENT_PRIVACY:
    case UPDATE_PAYMENT_DATA:
    case UPDATE_PRIVATE_AMBASSADOR_DATA:
      return getTimeNow();
    default:
      return state;
  }
}
