import {
  PrivateAmbassadorAction,
  CompanyAction,
} from "../../types/actions/MainData";
import { Company, BaseData } from "../../types/reduxInterfaces";
import { combineReducers } from "redux";
import { ambassadorTypeOptions } from "../../constants/mainData";
import { AmbassadorType } from "../../types/AmbassadorType";
import {
  UPDATE_COMPANY,
  UPDATE_PRIVATE_AMBASSADOR_DATA,
  AMBASSADOR_TYPE_CHOSEN,
  CLEAR_REGISTRATION,
} from "../../constants/actions";
import { ClearRegistrationAction } from "../../types/actions/clearRegistration";

type State = {
  ambassadorType: AmbassadorType;
  privateAmbassador: Partial<BaseData> | null;
  company: Partial<Company> | null;
};

export const initialState: State = {
  ambassadorType: ambassadorTypeOptions.none,
  privateAmbassador: null,
  company: null,
};

const ambassadorType = (
  state = initialState.ambassadorType,
  action: CompanyAction | PrivateAmbassadorAction | ClearRegistrationAction
) => {
  switch (action.type) {
    case AMBASSADOR_TYPE_CHOSEN:
      return action.payload.value;
    case CLEAR_REGISTRATION:
    case "CLEAR_STORAGE": {
      return initialState.ambassadorType;
    }
    default:
      return state;
  }
};

const privateAmbassador = (
  state = initialState.privateAmbassador,
  action: PrivateAmbassadorAction | ClearRegistrationAction
) => {
  switch (action.type) {
    case UPDATE_PRIVATE_AMBASSADOR_DATA: {
      const valuePair = {
        [action.payload.key]: action.payload.value,
      };
      if (!state) {
        return valuePair;
      }

      return {
        ...state,
        ...valuePair,
      };
    }
    case CLEAR_REGISTRATION:
    case "CLEAR_STORAGE": {
      return initialState.privateAmbassador;
    }
    default:
      return state;
  }
};

const company = (
  state = initialState.company,
  action: CompanyAction | ClearRegistrationAction
) => {
  switch (action.type) {
    case UPDATE_COMPANY: {
      const valuePair = {
        [action.payload.key]: action.payload.value,
      };
      if (!state) {
        return valuePair;
      }

      return {
        ...state,
        ...valuePair,
      };
    }
    case CLEAR_REGISTRATION:
    case "CLEAR_STORAGE": {
      return initialState.company;
    }
    default:
      return state;
  }
};

export const getPrivateAmbassadorData = (state: State) =>
  state.privateAmbassador;
export const getCompanyData = (state: State) => state.company;
export const getAmbassadorType = (state: State) => state.ambassadorType;

export default combineReducers({
  company,
  ambassadorType,
  privateAmbassador,
});
