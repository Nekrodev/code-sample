import { Payment } from "../../types/reduxInterfaces";
import { PaymentAction } from "../../types/actions/Payment";
import { accountTypes } from "../../types/AccountType";
import {
  UPDATE_PAYMENT_DATA,
  PAYMENT_ACCOUNT_TYPE_CHOSEN,
  CLEAR_PAYMENT_DATA,
  CLEAR_REGISTRATION,
} from "../../constants/actions";
import { ClearRegistrationAction } from "../../types/actions/clearRegistration";

export const initialState: Payment = {
  bankAccount: "",
  paypalEmail: "",
  accountType: accountTypes.none,
};

const payment = (
  state = initialState,
  action: PaymentAction | ClearRegistrationAction
): Payment => {
  const newState = { ...state };
  switch (action.type) {
    case UPDATE_PAYMENT_DATA:
      if (state.accountType === accountTypes.Paypal) {
        newState.paypalEmail = action.payload.paymentData;
      } else if (state.accountType === accountTypes.BankAccount) {
        newState.bankAccount = action.payload.paymentData;
      }

      return newState;
    case PAYMENT_ACCOUNT_TYPE_CHOSEN:
      newState.accountType = action.payload.accountType;

      return newState;
    case CLEAR_REGISTRATION:
    case CLEAR_PAYMENT_DATA:
    case "CLEAR_STORAGE":
      return initialState;
    default:
      return state;
  }
};

export const getBankAccount = (state: Payment) => state.bankAccount;
export const getPaypallEmail = (state: Payment) => state.paypalEmail;
export const getAccountType = (state: Payment) => state.accountType;

export default payment;
