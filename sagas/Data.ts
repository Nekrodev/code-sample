import { takeLeading, call, put, all } from "@redux-saga/core/effects";
import { baseRouteNames } from "../../routing/Routes";
import {
  saveAmbassadorProfileData,
  getAmbassadorProfileData,
} from "../services/profileData";
import {
  profileDataSaved,
  saveProfileDataFailed,
  gotProfileData,
  getProfileDataFailed,
} from "../actions/data";
import { SaveProfileDataAction } from "../types/actions/data";
import { SAVE_PROFILE_DATA, GET_PROFILE_DATA } from "../constants/actions";
import { navigate } from "../../routing/useNavigate";
import { isUnAuthorized } from "../../common/helpers";

function* saveData(action: SaveProfileDataAction) {
  try {
    const { payload } = action;
    yield call(saveAmbassadorProfileData, payload);
    yield put(profileDataSaved(payload));
  } catch (e) {
    yield put(saveProfileDataFailed(e));
  }
}

function* getData() {
  try {
    const result = yield call(getAmbassadorProfileData);
    yield put(gotProfileData(result));
  } catch (e) {
    yield put(getProfileDataFailed(e));
    if (isUnAuthorized(e)) {
      navigate(baseRouteNames.login, { clearLogin: true });
    }
  }
}

export function* profileData() {
  yield all([
    takeLeading(GET_PROFILE_DATA, getData),
    takeLeading(SAVE_PROFILE_DATA, saveData),
  ]);
}
