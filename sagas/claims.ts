import { takeLeading, call, put } from "@redux-saga/core/effects";
import { baseRouteNames } from "../../routing/Routes";
import { GET_CLAIMS_DATA } from "../constants/actions";
import { getAmbassadorClaimsData } from "../services/claim";
import { GetClaimsDataAction } from "../types/actions/AmbassadorClaims";
import { gotClaimsData, getClaimsDataFailed } from "../actions/claims";
import { navigate } from "../../routing/useNavigate";
import { isUnAuthorized } from "../../common/helpers";

function* getClaimsSaga(action: GetClaimsDataAction) {
  try {
    const result = yield call(
      getAmbassadorClaimsData,
      action.payload.requestParams
    );
    yield put(gotClaimsData(result.claims, result.totalSize));
  } catch (e) {
    yield put(getClaimsDataFailed(e));
    if (isUnAuthorized(e)) {
      navigate(baseRouteNames.login);
    }
  }
}

export function* getClaims() {
  yield takeLeading(GET_CLAIMS_DATA, getClaimsSaga);
}
