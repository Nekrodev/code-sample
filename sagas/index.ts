import { all } from "@redux-saga/core/effects";
import { profileData } from "./Data";
import { recommendations } from "./recommendations";
import { getClaims } from "./claims";

export default function* rootSaga() {
  yield all([profileData(), recommendations(), getClaims()]);
}
