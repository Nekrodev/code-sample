import { takeLeading, call, put, all } from "@redux-saga/core/effects";
import { baseRouteNames } from "../../routing/Routes";
import {
  GET_RECOMMENDATIONS_DATA,
  DELETE_RECOMMENDATION_ROW,
} from "../constants/actions";
import {
  gotRecommendationData,
  getRecommendationDataFailed,
  recommendationRowDeleted,
  deleteRecommendationRowFailed,
} from "../actions/recommendations";
import { GetRecommendationsDataAction } from "../types/actions/AmbassadorRecommendation";
import {
  getAmbassadorRecommendationData,
  deleteRecommendation,
} from "../services/recommendation";
import { DeleteRecommendationRowAction } from "../types/actions/recommendationsTableSettings";
import { navigate } from "../../routing/useNavigate";
import { isUnAuthorized } from "../../common/helpers";

function* getRecommendationsSaga(action: GetRecommendationsDataAction) {
  try {
    const result = yield call(
      getAmbassadorRecommendationData,
      action.payload.requestParams
    );
    yield put(gotRecommendationData(result.recommendations, result.totalSize));
  } catch (e) {
    yield put(getRecommendationDataFailed(e));
    if (isUnAuthorized(e)) {
      navigate(baseRouteNames.login);
    }
  }
}

function* DeleteRecommendation(action: DeleteRecommendationRowAction) {
  try {
    const id = action.payload.id;
    yield call(deleteRecommendation, id);
    yield put(recommendationRowDeleted(id));
  } catch (e) {
    yield put(deleteRecommendationRowFailed(e));
    if (isUnAuthorized(e)) {
      navigate(baseRouteNames.login);
    }
  }
}

export function* recommendations() {
  yield all([
    takeLeading(GET_RECOMMENDATIONS_DATA, getRecommendationsSaga),
    takeLeading(DELETE_RECOMMENDATION_ROW, DeleteRecommendation),
  ]);
}
