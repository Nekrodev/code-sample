import { RootState } from "../reducers";
import * as fromClaims from "../reducers/claims";

export const getClaims = (state: RootState) =>
  fromClaims.getClaims(state.claims);
export const getClaimsTableSettings = (state: RootState) =>
  fromClaims.getTableSettings(state.claims);
export const getClaimsColumnsVisibility = (state: RootState) =>
  fromClaims.getColumnsVisibility(state.claims);
export const getClaimsLoadingState = (state: RootState) =>
  fromClaims.getLoadingState(state.claims);
