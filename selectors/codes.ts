import { RootState } from "../reducers";
import * as fromCodes from "../reducers/codes";

export const getAmbassadorCodes = (state: RootState) =>
  fromCodes.getAmbassadorCodes(state.codes);
