import { RootState } from "../reducers";
import * as fromProfile from "../reducers/profile";
import { BaseData } from "../types/reduxInterfaces";

export const getCompanyData = (state: RootState) =>
  fromProfile.getCompanyData(state.profile);
export const getPrivateAmbassadorData = (state: RootState) =>
  fromProfile.getPrivateAmbassadorData(state.profile);
export const getAmbassadorType = (state: RootState) =>
  fromProfile.getAmbassadorType(state.profile);

export const getPaymentData = (state: RootState) =>
  fromProfile.getPaymentData(state.profile);
export const getPaymentAccountType = (state: RootState) =>
  fromProfile.getPaymentAccountType(state.profile);

export const getAmbassadorBaseData = (
  state: RootState
): Partial<BaseData> | null => fromProfile.getAmbassadorBaseData(state.profile);

export const getSavingStatus = (state: RootState) =>
  fromProfile.getSavingStatus(state.profile);
export const getLoadingStatus = (state: RootState) =>
  fromProfile.getLoadingStatus(state.profile);
