import { RootState } from "../reducers";
import * as fromRecommendation from "../reducers/recommendations";

export const getRecommendations = (state: RootState) =>
  fromRecommendation.getRecommendations(state.recommendation);
export const getRecommendationsTableSettings = (state: RootState) =>
  fromRecommendation.getTableSettings(state.recommendation);
export const getResendEmailModalState = (state: RootState) =>
  fromRecommendation.getResendEmailModalState(state.recommendation);
export const getRecommendationsColumnsVisibility = (state: RootState) =>
  fromRecommendation.getColumnsVisibility(state.recommendation);
export const getRecommendationsLoadingState = (state: RootState) =>
  fromRecommendation.getLoadingState(state.recommendation);
