import { RootState } from "../reducers";
import * as fromRegistration from "../reducers/registration";
import { AmbassadorProfileData, BaseData } from "../types/reduxInterfaces";

export const getLoadingStatus = (state: RootState) =>
  fromRegistration.getLoadingStatus(state.registration);
export const getCompanyData = (state: RootState) =>
  fromRegistration.getCompanyData(state.registration);
export const isCountryIdFilled = (state: RootState) =>
  fromRegistration.isCountryIdFilled(state.registration);
export const getPrivateAmbassadorData = (state: RootState) =>
  fromRegistration.getPrivateAmbassadorData(state.registration);
export const getAmbassadorType = (state: RootState) =>
  fromRegistration.getAmbassadorType(state.registration);

export const getPaymentData = (state: RootState) =>
  fromRegistration.getPaymentData(state.registration);
export const getPaymentAccountType = (state: RootState) =>
  fromRegistration.getPaymentAccountType(state.registration);

export const getAmbassadorCode = (state: RootState) =>
  fromRegistration.getAmbassadorCode(state.registration);

export const getConsentLegal = (state: RootState) =>
  fromRegistration.getConsentLegal(state.registration);
export const getConsentPrivacy = (state: RootState) =>
  fromRegistration.getConsentPrivacy(state.registration);

export const getAmbassadorBaseData = (
  state: RootState
): Partial<BaseData> | null =>
  fromRegistration.getAmbassadorBaseData(state.registration);

export const getAmbassadorProfileData = (
  state: RootState
): AmbassadorProfileData =>
  fromRegistration.getAmbassadorProfileData(state.registration);
