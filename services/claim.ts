import Axios from "axios";
import urls from "../constants/urls";
import { GetClaimsRequestParams } from "../types/reduxInterfaces";
import { AmbassadorClaim } from "../types/AmbassadorClaim";
import { formatRequestParams } from "./formatters";
import { tableFieldsMapping } from "../constants/table";
import { getOkResponse } from "../../common/services/Service";

export async function getAmbassadorClaimsData(
  requestParams: GetClaimsRequestParams
) {
  const result = await Axios.post(
    urls.claims,
    formatRequestParams(requestParams, tableFieldsMapping)
  );
  getOkResponse(result, "Could not parse Claims Data response");
  const claimsData = result.data;

  const packedClaimData: AmbassadorClaim[] = (claimsData.data || []).map(
    (value) => ({
      id: value.id,
      amount: value.potential_amount.toFixed(2),
      ambassadorCode: value.ambassador_code,
      flightDate: value.flight_date,
      reference: value.internal_reference,
      passengerName: value.passenger_name,
      route: value.flight_route,
      phase: {
        id: value.claim_phase.id,
        translation: value.claim_phase.translation.title,
      },
    })
  );
  const totalSize = claimsData.total_count || 0;

  return {
    claims: packedClaimData,
    totalSize,
  };
}
