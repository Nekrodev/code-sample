import { GetClaimsRequestParams } from "../types/reduxInterfaces";
import { tableFieldsMapping } from "../constants/table";
import { unifyDateNumber } from "../../common/services/unifyDateNumber";

const formatDate = (date: string) => {
  if (date.includes("/")) {
    // Means eng format
    return date
      .split("/")
      .filter((value) => !!value && value !== "0")
      .map(unifyDateNumber)
      .join("-");
  }

  if (date.includes(".")) {
    // Means de format
    return date
      .split(".")
      .filter((value) => !!value && value !== "0")
      .reverse()
      .map(unifyDateNumber)
      .join("-");
  }

  return date;
};

export const formatRequestParams = (
  requestParams: GetClaimsRequestParams,
  keys: any
) => ({
  order_by: requestParams.orderBy
    ? keys[requestParams.orderBy]
    : keys.flightDate,
  order_dir: requestParams.orderDirection,
  limit: requestParams.recordsPerPage,
  page: requestParams.page,
  properties: (requestParams.filters || []).reduce((total, current) => {
    if (current.key === tableFieldsMapping.flightDate) {
      total[current.key] = formatDate(current.value);
    } else {
      total[current.key] = current.value;
    }

    return total;
  }, {}),
});
