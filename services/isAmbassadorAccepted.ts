import Axios from "axios";
import urls from "../constants/urls";
import { getOkResponse } from "../../common/services/Service";

export async function isAmbassadorAccepted() {
  const result = await Axios.get(urls.isAmbassadorActive);
  getOkResponse(result, "Can not parse response");

  return !!result.data.data.is_accepted;
}
