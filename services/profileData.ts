import Axios from "axios";
import { getOkResponse, Response } from "../../common/services/Service";
import urls from "../../common/constants/urls";
import {
  AmbassadorProfileData,
  Payment,
  BaseData,
  Company,
} from "../types/reduxInterfaces";
import { accountTypes } from "../types/AccountType";
import { ambassadorTypeOptions } from "../constants/mainData";
import splitZipCity from "../../common/services/splitZipCity";

const formatPersonalData = (personalData: Partial<BaseData> | null) => {
  if (!personalData) {
    return null;
  }

  return {
    first_name: personalData.firstName,
    last_name: personalData.lastName,
    phone_number: personalData.phone,
  };
};

const formatAddress = (address: Partial<BaseData> | null) => {
  if (!address) {
    return null;
  }
  const result = {
    street_house: address.street,
    country: address.countryId,
  };
  if (address.zipCity) {
    const [zip, city] = splitZipCity(address.zipCity);
    result["zip"] = zip;
    result["city"] = city;
  }

  return result;
};

const formatPayment = (payment: Partial<Payment> | null) => {
  if (payment && payment.accountType === accountTypes.Paypal) {
    return {
      account_type_id: payment.accountType,
      account_number: payment.paypalEmail,
    };
  }
  if (payment && payment.accountType === accountTypes.BankAccount) {
    return {
      account_type_id: payment.accountType,
      account_number: payment.bankAccount,
    };
  }

  return null;
};

const formatCompany = (company: Partial<Company> | null) => {
  if (!company) {
    return null;
  }

  return {
    company_name: company.companyName,
    taxidva: company.taxId,
    website: company.website,
  };
};

const getProfileData = (data: AmbassadorProfileData) => {
  if (data.ambassadorType === ambassadorTypeOptions.Company) {
    return data.company;
  }
  if (data.ambassadorType === ambassadorTypeOptions.PrivateAmbassador) {
    return data.privateAmbassador;
  }

  return null;
};

const format = (data: AmbassadorProfileData) => {
  return {
    profile: formatPersonalData(getProfileData(data)),
    address: formatAddress(getProfileData(data)),
    payment_account: formatPayment(data.payment),
    company: formatCompany(data.company),
  };
};

let recentData: Partial<AmbassadorProfileData> | null = null;

export async function saveAmbassadorProfileData(
  profile: AmbassadorProfileData
) {
  if (recentData && recentData === profile) {
    return null;
  }
  const result = await Axios.post(urls.profile, format(profile));
  const data = getOkResponse<Response>(
    result,
    "Could not parse profile save response"
  );
  recentData = profile;

  return data;
}

const getPaymentAccountData = (paymentAccount: any): Payment | null => {
  if (paymentAccount.account_type_id === accountTypes.BankAccount) {
    return {
      accountType: accountTypes.BankAccount,
      bankAccount: paymentAccount.account_number,
      paypalEmail: "",
    };
  }

  if (paymentAccount.account_type_id === accountTypes.Paypal) {
    return {
      accountType: accountTypes.Paypal,
      bankAccount: "",
      paypalEmail: paymentAccount.account_number,
    };
  }

  return null;
};

export async function getAmbassadorProfileData() {
  const result = await Axios.get(urls.profile);
  getOkResponse(result, "Could not parse Profile Data response");
  const profileData = result.data;

  if (!!profileData.data.company) {
    const packedProfileData: AmbassadorProfileData = profileData.data && {
      ambassadorType: ambassadorTypeOptions.Company,
      company: {
        firstName: profileData.data.profile.first_name,
        lastName: profileData.data.profile.last_name,
        phone: profileData.data.profile.phone_number,
        email: profileData.data.email,
        street: profileData.data.address.street_house,
        zipCity:
          profileData.data.address.zip + " " + profileData.data.address.city,
        countryId: profileData.data.address.country,
        companyName: profileData.data.company.company_name,
        taxId: profileData.data.company.taxidva,
        website: profileData.data.company.website,
        repeatedEmail: profileData.data.email,
      },
      privateAmbassador: null,
      payment: getPaymentAccountData(profileData.data.payment_account),
    };

    return packedProfileData;
  }

  const packedProfileData: AmbassadorProfileData = profileData.data && {
    ambassadorType: ambassadorTypeOptions.PrivateAmbassador,
    privateAmbassador: {
      firstName: profileData.data.profile.first_name,
      lastName: profileData.data.profile.last_name,
      email: profileData.data.email,
      street: profileData.data.address.street_house,
      zipCity:
        profileData.data.address.zip + " " + profileData.data.address.city,
      countryId: profileData.data.address.country,
      repeatedEmail: profileData.data.email,
      phone: profileData.data.profile.phone_number,
    },
    company: null,
    payment: getPaymentAccountData(profileData.data.payment_account),
  };

  return packedProfileData;
}
