import Axios from "axios";
import urls from "../constants/urls";
import commonUrls from "../../common/constants/urls";
import { AmbassadorRecommendation } from "../types/AmbassadorRecommendation";
import { GetClaimsRequestParams } from "../types/reduxInterfaces";
import { tableFieldsMapping } from "../constants/table";
import { getOkResponse } from "../../common/services/Service";
import { formatRequestParams } from "./formatters";

export async function getAmbassadorRecommendationData(
  requestParams: GetClaimsRequestParams
) {
  const result = await Axios.post(
    urls.recommendations,
    formatRequestParams(requestParams, tableFieldsMapping)
  );
  getOkResponse(result, "Could not parse recommendation data response");
  const responseData = result.data;

  const packedRecommendationData: AmbassadorRecommendation[] = (
    responseData.data || []
  ).map((value) => ({
    id: value.id,
    amount: value.potential_amount.toFixed(2),
    email: value.passenger_email,
    flightDate: value.flight_date,
    reference: value.internal_reference,
    passengerName: value.passenger_name,
    route: value.flight_route,
    isSigned: value.is_used,
    recommendationId: value.recommendation_id,
  }));
  const totalSize = responseData.total_count || 0;

  return {
    recommendations: packedRecommendationData,
    totalSize,
  };
}

export async function deleteRecommendation(id: number) {
  const result = await Axios.post(urls.deleteRecommendation, { id });
  getOkResponse(result, "Could not parse Delete Recommendation response");
}

export async function resendRecommendation(
  recommendationId: number,
  email: string
) {
  const requestParams = {
    is_recommendation: true,
    id: recommendationId,
    passengers: [
      {
        email: email,
      },
    ],
  };

  const result = await Axios.post(commonUrls.saveClaim, requestParams);
  getOkResponse(result, "Could not parse resend recommendation response");
}
