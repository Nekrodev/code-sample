import urls from "../constants/urls";
import Axios from "axios";
import { getOkResponse, Response } from "../../common/services/Service";
import {
  getAmbassadorType,
  getCompanyData,
  getPrivateAmbassadorData,
  getConsentPrivacy,
  getConsentLegal,
  getAmbassadorCode,
  getPaymentData,
} from "../selectors/registration";
import { language } from "../../common/services/Language";
import { ambassadorTypeOptions } from "../constants/mainData";
import { accountTypes } from "../types/AccountType";
import { RootState } from "../reducers";

export async function registerAmbassador(state: RootState) {
  const result = await Axios.post(
    urls.registerAmbassador,
    getAmbassadorRegistrationData(state)
  );

  return getOkResponse<Response>(
    result,
    "could not parse registerAmbassador response"
  );
}

export async function resendAmbassadorActivation(state: RootState) {
  const requestParams = getAmbassadorRegistrationData(state);
  requestParams["resend_activation"] = true;
  const result = await Axios.post(urls.registerAmbassador, requestParams);

  return getOkResponse<Response>(
    result,
    "could not parse resend email response"
  );
}

const getRequestMainData = (state: RootState) => {
  const data = {};
  const ambassadorType = getAmbassadorType(state);
  const companyData = getCompanyData(state);
  if (companyData && ambassadorType === ambassadorTypeOptions.Company) {
    data["company_name"] = companyData.companyName;
    data["taxidva"] = companyData.taxId;
    data["website"] = companyData.website;
    data["street_house"] = companyData.street;
    data["country"] = companyData.countryId;
    data["email"] = companyData.email;
    data["phone_number"] = companyData.phone;
    data["first_name"] = companyData.firstName;
    data["last_name"] = companyData.lastName;

    if (companyData.zipCity) {
      const [zip, ...city] = companyData.zipCity.split(" ");
      data["postal_code"] = zip;
      data["city"] = city.join(" ");
    }
  }

  const privateAmbassadorData = getPrivateAmbassadorData(state);
  if (
    privateAmbassadorData &&
    ambassadorType === ambassadorTypeOptions.PrivateAmbassador
  ) {
    data["street_house"] = privateAmbassadorData.street;
    data["country"] = privateAmbassadorData.countryId;
    data["email"] = privateAmbassadorData.email;
    data["phone_number"] = privateAmbassadorData.phone;
    data["first_name"] = privateAmbassadorData.firstName;
    data["last_name"] = privateAmbassadorData.lastName;

    if (privateAmbassadorData.zipCity) {
      const [zip, ...city] = privateAmbassadorData.zipCity.split(" ");
      data["postal_code"] = zip;
      data["city"] = city.join(" ");
    }
  }

  return data;
};

const getRequestPaymentData = (state: RootState) => {
  const data = {};
  const paymentData = getPaymentData(state);
  if (
    paymentData.accountType === accountTypes.BankAccount &&
    paymentData.bankAccount
  ) {
    data["payment_account_type_id"] = paymentData.accountType;
    data["payment_account_number"] = paymentData.bankAccount;
  }

  if (
    paymentData.accountType === accountTypes.Paypal &&
    paymentData.paypalEmail
  ) {
    data["payment_account_type_id"] = paymentData.accountType;
    data["payment_account_number"] = paymentData.paypalEmail;
  }

  return data;
};

export const getAmbassadorRegistrationData = (state: RootState) => {
  const data = {
    ...getRequestMainData(state),
    ...getRequestPaymentData(state),
  };

  data["language"] = language();
  data["type_id"] = getAmbassadorType(state);
  data["consent_privacy"] = getConsentPrivacy(state);
  data["consent_legal"] = getConsentLegal(state);

  data["ambassador_code"] = getAmbassadorCode(state);

  return data;
};

export async function validateAmbassadorRegistrationData(
  state: RootState,
  withPayment: boolean
) {
  const requestParams = withPayment
    ? {
        ...getRequestMainData(state),
        ...getRequestPaymentData(state),
      }
    : getRequestMainData(state);
  const result = await Axios.post(
    urls.validateAmbassadorRegistrationData,
    requestParams
  );

  return getOkResponse<Response>(result, "could not parse validation response");
}
