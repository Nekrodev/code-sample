import React from "react";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import createSagaMiddleware from "@redux-saga/core";
import rootReducer from "../reducers";
import { PersistGate } from "redux-persist/integration/react";
import { getTimeNow } from "../../common/services/Time";
import { SESSION_LIFETIME } from "../../common/constants/appConfig";
import sagas from "../sagas";
import { clearRegistration } from "../actions/clearRegistration";
import { clearStorageCache } from "../../common/services/clearStorage";
import { ClearStorageOptions } from "../../common/types/actions/clearStorage";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";

const persistConfig = {
  key: "ambassador",
  storage,
  stateReconciler: autoMergeLevel2,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();
const createEnhancedStore = applyMiddleware(sagaMiddleware)(createStore);

export const store = createEnhancedStore(persistedReducer);
const persistor = persistStore(store, undefined, () =>
  maybeClearAmbassadorStorage()
);
sagaMiddleware.run(sagas);

const PersistedReduxStore: React.FC = (props) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      {props.children}
    </PersistGate>
  </Provider>
);

export const clearAmbassadorStorage = (options?: ClearStorageOptions) => {
  clearStorageCache(store, persistor, options);
};

export const maybeClearAmbassadorStorage = () => {
  const isStorageDataExpired =
    getTimeNow() - store.getState().registration.lastUpdate > SESSION_LIFETIME;
  const isStorageCleared =
    !store.getState().registration.mainData.company &&
    !store.getState().registration.mainData.privateAmbassador; //  optimization: we will not clear persistent storage if it is already cleared

  if (isStorageDataExpired && !isStorageCleared) {
    store.dispatch(clearRegistration());
  }
};

export default PersistedReduxStore;
