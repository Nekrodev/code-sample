export enum accountTypes {
  none = 1,
  Paypal = 2,
  BankAccount = 3,
}

export type AccountType =
  | accountTypes.Paypal
  | accountTypes.BankAccount
  | accountTypes.none;
