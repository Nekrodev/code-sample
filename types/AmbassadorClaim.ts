import { TranslationUnit } from "../../common/types/Translations";

export interface AmbassadorClaim {
  id: number;
  amount: number;
  ambassadorCode: string;
  flightDate: string;
  reference: string;
  passengerName: string;
  route: string;
  phase: {
    id: number;
    translation: TranslationUnit;
  };
}
