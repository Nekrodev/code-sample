export interface AmbassadorCode {
  code: string;
  id: number;
}
