export interface AmbassadorRecommendation {
  id: number;
  amount: number;
  email: string;
  flightDate: string;
  reference: string;
  passengerName: string;
  route: string;
  isSigned: boolean;
  recommendationId: number;
}

export type OrderDirection = "asc" | "desc";

export type PaginationOption = 20 | 50 | 100;
