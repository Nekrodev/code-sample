import { ambassadorTypeOptions } from "../constants/mainData";
export type AmbassadorType =
  | ambassadorTypeOptions.Company
  | ambassadorTypeOptions.PrivateAmbassador
  | ambassadorTypeOptions.none;
