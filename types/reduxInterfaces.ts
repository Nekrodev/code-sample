import { AccountType } from "./AccountType";
import { AmbassadorType } from "./AmbassadorType";

export interface ColumnVisibilityState {
  amount: boolean;
  flightDate: boolean;
  passengerName: boolean;
  flightRoute: boolean;
  reference: boolean;
  status: boolean;
}

export interface RecommendationsColumnsVisibilityState
  extends ColumnVisibilityState {
  actions: boolean;
}

export interface ClaimsColumnsVisibilityState extends ColumnVisibilityState {
  ambassadorCode: boolean;
}

export interface BaseData {
  firstName: string;
  lastName: string;
  email: string;
  phone?: string;
  street: string;
  zipCity: string;
  countryId: string;
  repeatedEmail: string;
  donationPercent: number;
}

export interface Company extends BaseData {
  companyName: string;
  taxId: string;
  website?: string;
}

export interface Payment {
  paypalEmail: string;
  bankAccount: string;
  accountType: AccountType;
}

export interface DonationSetting {
  donationPercent: number;
}

export interface AmbassadorProfileData {
  ambassadorType: AmbassadorType;
  privateAmbassador: Partial<BaseData> | null;
  company: Partial<Company> | null;
  payment: Partial<Payment> | null;
}

export interface GetClaimsRequestParams {
  recordsPerPage: number;
  orderBy: string;
  orderDirection: string;
  page: number;
  filters: { key: string; value: string }[];
}

export interface TableSettings extends GetClaimsRequestParams {
  totalSize: number;
}
